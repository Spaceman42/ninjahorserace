package com.ten.track;

import org.andengine.util.debug.Debug;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Transform;

public class TrackFollower2 {
	
	
	private static int MIN = -400;
	private static int MAX = 400;
	
	private Track track;
	private float t;
	private Vector2 location;
	
	
	
	
	public TrackFollower2(Track track, float initialT) {
		this.track = track;
		location = track.getValue(initialT);
	}

	public Transform nextTransform(float speed, float offset) {
		Vector2 oldLoc = location;
		t = track.calculateParameter(speed, t, offset);
		location = track.getValue(t, offset);
		Vector2 tan = track.getTangent(t);
		System.out.println(oldLoc.sub(location).len());
		//if ()
		return new Transform(convertLoc(location), 
				(float) (Math.atan2(-tan.y, tan.x) - Math.PI / 2.0));
	}
	
	private Vector2 convertLoc(Vector2 loc) {
		float x = (4*loc.x) + 1600;
		float y = -(4*loc.y) + 1600;
		return new Vector2(4*x, 4*y);
	}
}
