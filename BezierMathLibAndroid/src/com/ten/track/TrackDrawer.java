package com.ten.track;

import java.util.ArrayList;
import java.util.List;

import org.andengine.entity.primitive.Line;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.color.Color;
import org.andengine.util.debug.Debug;

import com.badlogic.gdx.math.Vector2;
import com.ten.curve.CubicCurve;

public class TrackDrawer {
	
	private List<Line> lines;
	
	private final Vector2 min;
	private final Vector2 max;
	
	private float scale;
	
	public TrackDrawer(Track track, float mapWidth, float mapHeight, VertexBufferObjectManager vbom) {
		lines = new ArrayList<Line>();
		min = new Vector2(track.getMinX(), track.getMinY());
		max = new Vector2(track.getMaxX(), track.getMaxY());
		
		scale = max.x/mapWidth;
		
		for (CubicCurve c : track.getCurves()) {
			List<Vector2> points = c.createPointList(50f, 0f);
			for (int i = 0; i < points.size() - 1; i++) {
				if (points.get(i) == null) {
					Debug.e("Point is null!");
					System.exit(-1);
				}
				Vector2 vec1 = adjustCoord(new Vector2(points.get(i).x, points.get(i).y));
				Vector2 vec2 = adjustCoord(new Vector2(points.get(i + 1).x, points.get(i + 1).y));
				
				Line line = new Line(vec1.x, vec1.y, vec2.x, vec2.y, vbom);
				line.setColor(Color.BLUE);
				lines.add(line);
			}
		}
		
		
	}
	private Vector2 adjustCoord(Vector2 loc) {
		float x = (4*loc.x) + 1600;
		float y = -(4*loc.y) + 1600;
		return new Vector2(x, y);
	}
	public List<Line> getLines() {
		return lines;
	}
}
