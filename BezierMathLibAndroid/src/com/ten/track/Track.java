package com.ten.track;

import java.io.IOException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import com.badlogic.gdx.math.Vector2;
import com.ten.curve.CubicCurve;
import com.ten.curve.util.BoundingBox;

public interface Track {
	
	public float getMinX();
	public float getMinY();
	public float getMaxX();
	public float getMaxY();
	
	public float getOffset();
	public void setOffset(float offset);
	
	public float calculateParameter(float distance, float start, float offset);
	
	public List<Vector2> createPointList(float error, float offset);
	
	public List<CubicCurve> getCurves();
	
	public boolean isInsideTrack(Vector2 point, float error, float offset);
	
	public Vector2 getValue(float t);
	public Vector2 getValue(float t, float offset);
	public Vector2 getTangent(float t);
	
	public void updateBound(BoundingBox boundingBox);
	
	public String exportToXml()
			throws TransformerException, ParserConfigurationException;
	public void importFromXml(String xml) 
			throws SAXException, IOException, ParserConfigurationException;
}
