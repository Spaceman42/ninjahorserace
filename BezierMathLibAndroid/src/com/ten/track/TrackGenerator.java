package com.ten.track;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.math.Vector2;
import com.ten.curve.CurveControl;
import com.ten.curve.util.MathUtils;

public class TrackGenerator {

	private List<CurveControl> controls;
	private TestTrack track;
	
	public TrackGenerator() {
		track = new TestTrack();
	}
	
	public Track getTrack() {
		return track;
	}
	
	public List<CurveControl> getControls() {
		return controls;
	}
	
	public CurveControl getNearestCurveControl(Vector2 v, float radius) {
		for (CurveControl cc : controls) {
			if (new Vector2(cc.controlPoint).sub(v).len() <= radius) {
				return cc;
			}
		}
		return null;
	}
	
	public CurveControl getNearestCurveControlBack(Vector2 v, float radius) {
		for (CurveControl cc : controls) {
			if (new Vector2(cc.getEndpointBack()).sub(v).len() <= radius) {
				return cc;
			}
		}
		return null;
	}
	
	public CurveControl getNearestCurveControlFront(Vector2 v, float radius) {
		for (CurveControl cc : controls) {
			if (new Vector2(cc.getEndpointFront()).sub(v).len() <= radius) {
				return cc;
			}
		}
		return null;
	}
	
	public void createInitialTrack(int numPoints, float radius) {
		float angle = 0.0f;
		controls = new ArrayList<CurveControl>();
		for (int i = 0; i < numPoints; i++) {
			controls.add(new CurveControl((float) Math.cos(angle) * radius,
										   (float) Math.sin(angle) * radius,
										   MathUtils.fixAngle((float) (angle + Math.PI / 2.0)),
										   40f, 40f));
			angle += (Math.PI * 2.0f) / (float) numPoints;
		}
		track.generateFromControls(controls);
	}
	
	public void regenerateTrack() {
		track.generateFromControls(controls);
	}
}
