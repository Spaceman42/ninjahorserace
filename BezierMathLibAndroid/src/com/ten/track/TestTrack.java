package com.ten.track;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.badlogic.gdx.math.Vector2;
import com.ten.curve.CubicCurve;
import com.ten.curve.CurveControl;
import com.ten.curve.util.BoundingBox;
import com.ten.curve.util.MathUtils;

public class TestTrack implements Track {

	private static final float PARAMETER_ERROR = .1f;
	
	private float minX;
	private float minY;
	private float maxX;
	private float maxY;
	
	private List<CubicCurve> curves;
	
	private float offset;
	
	public TestTrack() {
	}
	
	public float getOffset() {
		return offset;
	}
	
	public void setOffset(float offset) {
		this.offset = offset;
	}
	
	public Vector2 getValue(float t) {
		t %= (float) curves.size();
		return curves.get((int) t).getValue(t % 1.0f);
	}
	
	public Vector2 getValue(float t, float offset) {
		t %= (float) curves.size();
		return curves.get((int) t).getValue(t % 1.0f, offset);
	}
	
	public Vector2 getTangent(float t) {
		t %= (float) curves.size();	
		return curves.get((int) t).getTangent(t % 1.0f);
	}
	
	public void generateFromControls(List<CurveControl> controls) {
		curves = new ArrayList<CubicCurve>();
		for (int i = 0; i < controls.size(); i++) {
			if (i != controls.size() - 1)
				curves.add(new CubicCurve(controls.get(i).controlPoint, 
						controls.get(i).getEndpointFront(), controls.get(i + 1).getEndpointBack(), 
						controls.get(i + 1).controlPoint));
			else
				curves.add(new CubicCurve(controls.get(i).controlPoint, 
						controls.get(i).getEndpointFront(), controls.get(0).getEndpointBack(), 
						controls.get(0).controlPoint));
		}
		calculateBounds();
	}
	
	
	public List<Vector2> createPointList(float error, float offset) {
		List<Vector2> points = new ArrayList<Vector2>();
		for (CubicCurve cc : curves) {
			points.addAll(cc.createPointList(error, offset));
		}
		return points;
	}

	public void calculateBounds() {
		setMinX(Integer.MAX_VALUE);
		setMinY(Integer.MAX_VALUE);
		setMaxX(Integer.MIN_VALUE);
		setMaxY(Integer.MIN_VALUE);
		
		for (CubicCurve cc : curves) {
			if (cc.p0.x < getMinX())
				setMinX(cc.p0.x);
			if (cc.p1.x < getMinX())
				setMinX(cc.p1.x);
			if (cc.p2.x < getMinX())
				setMinX(cc.p2.x);
			if (cc.p3.x < getMinX())
				setMinX(cc.p3.x);
		}

		for (CubicCurve cc : curves) {
			if (cc.p0.y < getMinY())
				setMinY(cc.p0.y);
			if (cc.p1.y < getMinY())
				setMinY(cc.p1.y);
			if (cc.p2.y < getMinY())
				setMinY(cc.p2.y);
			if (cc.p3.y < getMinY())
				setMinY(cc.p3.y);
		}
		
		for (CubicCurve cc : curves) {
			if (cc.p0.x > getMaxX())
				setMaxX(cc.p0.x);
			if (cc.p1.x > getMaxX())
				setMaxX(cc.p1.x);
			if (cc.p2.x > getMaxX())
				setMaxX(cc.p2.x);
			if (cc.p3.x > getMaxX())
				setMaxX(cc.p3.x);
		}
		
		for (CubicCurve cc : curves) {
			if (cc.p0.y > getMaxY())
				setMaxY(cc.p0.y);
			if (cc.p1.y > getMaxY())
				setMaxY(cc.p1.y);
			if (cc.p2.y > getMaxY())
				setMaxY(cc.p2.y);
			if (cc.p3.y > getMaxY())
				setMaxY(cc.p3.y);
		}
	}

	
	public float getMinX() {
		return minX;
	}
	
	
	public float getMinY() {
		return minY;
	}

	
	public float getMaxX() {
		return maxX;
	}

	
	public float getMaxY() {
		return maxY;
	}

	
	public List<CubicCurve> getCurves() {
		return curves;
	}

	
	public boolean isInsideTrack(Vector2 point, float error, float offset) {
		return MathUtils.intersects(createPointList(error, 0), new Vector2(), point) && 
			   !MathUtils.intersects(createPointList(error, offset), new Vector2(), point);
	}

	public void setMinX(float minX) {
		this.minX = minX;
	}

	public void setMinY(float minY) {
		this.minY = minY;
	}

	public void setMaxX(float maxX) {
		this.maxX = maxX;
	}

	public void setMaxY(float maxY) {
		this.maxY = maxY;
	}

	
	public void updateBound(BoundingBox bb) {
		minX = bb.getMinX();
		minY = bb.getMinY();
		maxX = bb.getMaxX();
		maxY = bb.getMaxY();
	}
	
	public float calculateParameter(float distance, float start, float offset) {
		float l = getCurve(start).calculateLength(PARAMETER_ERROR, start % 1.0f, offset);
		while (distance > l) {
			distance -= l;
			start = getNextCurve(getCurve(start));
			l = getCurve(start).calculateLength(PARAMETER_ERROR, 0, offset);
		}
		return getCurve(start).calculateParameter(PARAMETER_ERROR, distance, start % 1.0f, offset) + ((int) start);
	}
	
	public int getNextCurve(CubicCurve cc) {
		int i = curves.indexOf(cc);
		if (i == -1) // Curve not found
			return -1;
		else if (i >= curves.size() - 1) // Curve at very end
			return 0;
		else
			return i + 1; // Next Curve
	}
	
	public CubicCurve getCurve(float t) {
		return curves.get(((int) t) % curves.size());
	}

	
	public String exportToXml()
			throws TransformerException, ParserConfigurationException {
		Document doc = DocumentBuilderFactory
				.newInstance()
				.newDocumentBuilder()
				.newDocument();
		
		Element root = doc.createElement("track");
		doc.appendChild(root);
		
		Element min = doc.createElement("min");
		min.setAttribute("x", Float.toString(minX));
		min.setAttribute("y", Float.toString(minY));
		root.appendChild(min);
		
		Element max = doc.createElement("max");
		max.setAttribute("x", Float.toString(maxX));
		max.setAttribute("y", Float.toString(maxY));
		root.appendChild(max);
		
		Element o = doc.createElement("offset");
		o.setTextContent(Float.toString(offset));
		root.appendChild(o);
		
		for (CubicCurve cc : getCurves()) {
			Element curve = doc.createElement("curve");
			root.appendChild(curve);
			
			Element p0 = doc.createElement("controlPoint");
			p0.setAttribute("x", Float.toString(cc.p0.x));
			p0.setAttribute("y", Float.toString(cc.p0.y));
			curve.appendChild(p0);
			
			Element p1 = doc.createElement("controlPoint");
			p1.setAttribute("x", Float.toString(cc.p1.x));
			p1.setAttribute("y", Float.toString(cc.p1.y));
			curve.appendChild(p1);
			
			Element p2 = doc.createElement("controlPoint");
			p2.setAttribute("x", Float.toString(cc.p2.x));
			p2.setAttribute("y", Float.toString(cc.p2.y));
			curve.appendChild(p2);
			
			Element p3 = doc.createElement("controlPoint");
			p3.setAttribute("x", Float.toString(cc.p3.x));
			p3.setAttribute("y", Float.toString(cc.p3.y));
			curve.appendChild(p3);
		}
		
		Transformer transformer = TransformerFactory
				.newInstance()
				.newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		
		DOMSource source = new DOMSource(doc);
		StringWriter sw = new StringWriter();
		transformer.transform(source, new StreamResult(sw));
		return sw.toString();
	}

	
	public void importFromXml(String xml) throws SAXException, IOException, ParserConfigurationException {
		curves = new ArrayList<CubicCurve>();
		
		Document doc = DocumentBuilderFactory
				.newInstance()
				.newDocumentBuilder()
				.parse(new ByteArrayInputStream(xml.getBytes()));
		Element root = doc.getDocumentElement();
		
		NodeList nl = root.getChildNodes();
		for (int i = 0; i<nl.getLength(); i++) {
			Node n = nl.item(i);
			if (n.getNodeType() == Node.ELEMENT_NODE) {
				Element element = (Element) n;
				if (element.getTagName().equals("min")) {
					minX = Float.parseFloat(element.getAttribute("x"));
					minY = Float.parseFloat(element.getAttribute("y"));
				} else if (element.getTagName().equals("max")) {
					maxX = Float.parseFloat(element.getAttribute("x"));
					maxY = Float.parseFloat(element.getAttribute("y"));
				} else if (element.getTagName().equals("offset")) {
					offset = Float.parseFloat(element.getTextContent());
				}
			}
		}

		// Iterate through every curve 
		NodeList curveNodes = root.getElementsByTagName("curve");
		for (int i = 0; i < curveNodes.getLength(); i++) {
			Node curveNode = curveNodes.item(i);
			if (curveNode.getNodeType() == Node.ELEMENT_NODE) {
				CubicCurve curve = new CubicCurve();
				
				// Iterate through every control point
				NodeList controlPointNodes = ((Element) curveNode)
						.getElementsByTagName("controlPoint");
				for (int j = 0; j < controlPointNodes.getLength(); j++) {
					Node controlPointNode = controlPointNodes.item(j);
					if (controlPointNode.getNodeType() == Node.ELEMENT_NODE) {
						Element controlPoint = ((Element) controlPointNode);
						curve.setPoint(j, new Vector2(Float.parseFloat(controlPoint.getAttribute("x")),
								Float.parseFloat(controlPoint.getAttribute("y"))));
					}
				}
				
				curves.add(curve);
			}
		}
	}
}
