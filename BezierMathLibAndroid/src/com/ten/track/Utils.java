package com.ten.track;

import com.badlogic.gdx.math.Vector2;

public class Utils {
	private Utils() {}
	
	
	public static Vector2 adjustCoord(Vector2 loc) {
		float x = (loc.x) + 32*50-400;
		float y = (loc.y) + 32*50-400;
		return new Vector2(x, y);
	}
}
