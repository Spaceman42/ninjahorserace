package com.ten.curve;

import com.badlogic.gdx.math.Vector2;

public class CurveControl {
	
	public Vector2 controlPoint;
	public float controlAngle;
	public float controlLength1;
	public float controlLength2;
		
	public CurveControl(float x, float y, float angle, 
			float length1, float length2) {
		controlPoint = new Vector2(x, y);
		controlAngle = angle;
		controlLength1 = length1;
		controlLength2 = length2;
	}
	
	public Vector2 getEndpointBack() {
		return new Vector2((float) Math.cos(controlAngle) * -controlLength1,
						   (float) Math.sin(controlAngle) * -controlLength1).add(controlPoint);
	}
	
	public Vector2 getEndpointFront() {
		return new Vector2((float) Math.cos(controlAngle) * controlLength2,
						   (float) Math.sin(controlAngle) * controlLength2).add(controlPoint);
	}
}