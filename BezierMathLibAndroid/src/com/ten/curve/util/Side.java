package com.ten.curve.util;

public class Side {
	public int min;
	public int max;
	public int numTiles;
	
	public Side(int min, int max) {
		this.max = max;
		this.min = min;
	}
	
	public int length() {
		return max - min;
	}
	
	public void growEven(int amount) {
		int halfAmount = amount/2;
		int remainder = amount % 2;
		
		min -= halfAmount;
		max += halfAmount + remainder;
	}
	
	public void growEvenTo(int newSize) {
		int amount = newSize - length();
		growEven(amount);
	}
	
	public boolean divisbleBy(int x) {
		return ((float)length() % x == 0);
	}
	
	public boolean isIntDivisibleBy(int x) {
		return (x % (float)length() == 0); 
	}
	
	public static Side largest(Side s0, Side s1) {
		if (s0.length() >= s1.length()) {
			return s0;
		} else {
			return s1;
		}
	}

	public static Side smallest(Side s0, Side s1) {
		if (s0.length() >= s1.length()) {
			return s1;
		} else {
			return s0;
		}
	}
}
