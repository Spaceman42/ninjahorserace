package com.ten.curve.util;

import com.ten.track.Track;

public class BoundingBox {
	
	public class ControlHandle {
		
		public int x;
		public int y;
		public int size;
		
		public ControlHandle(int x, int y, int size) {
			this.x = x;
			this.y = y;
			this.size = size;
		}
	}
	
	private static final int CONTROL_SIZE = 12;
	
	private int minX;
	private int minY;
	private int maxX;
	private int maxY;
	
	private ControlHandle[] controls;
	private int selectedControl = -1;
	
	public BoundingBox(int minX, int minY, int maxX, int maxY) {
		this.setMinX(minX);
		this.setMinY(minY);
		this.setMaxX(maxX);
		this.setMaxY(maxY);
		createControlHandles();
	}
	
	public BoundingBox(Track track) {
		regenFromTrack(track);
		createControlHandles();
	}
	
	private void createControlHandles() {
		controls = new ControlHandle[4];
		//Upper left
		controls[0] = new ControlHandle(minX, minY, CONTROL_SIZE);
		//Upper right
		controls[1] = new ControlHandle(maxX, minY, CONTROL_SIZE);
		//Lower right
		controls[2] = new ControlHandle(maxX, maxY, CONTROL_SIZE);
		//Lower left
		controls[3] = new ControlHandle(minX, maxY, CONTROL_SIZE);
	}
	
	public ControlHandle[] getControls() {
		return controls;
	}
	
	public void regenFromTrack(Track track) {
		setMinX((int)track.getMinX());
		setMinY((int)track.getMinY());
		setMaxX((int)track.getMaxX());
		setMaxY((int)track.getMaxY());
	}
	
	public void setClickedControl(int mouseX, int mouseY) {
		for (int i = 0; i<controls.length; i++) {
			ControlHandle ch = controls[i];
			int x = ch.x-ch.size/2;
			int y = ch.y-ch.size/2;
			
			if (Math.abs(mouseX - x) <= ch.size/2 && Math.abs(mouseY - y) <= ch.size/2) {
				selectedControl = i;
				return;
			}
		}
		selectedControl = -1;
	}
	
	public void mouseDragged(int mouseX, int mouseY) {
		if (selectedControl != -1) {
			ControlHandle control = controls[selectedControl];
			control.x = mouseX;
			control.y = mouseY;
			regenFromControls(selectedControl);
		}
	}
	private void regenFromControls(int changed) {
		switch (changed) {
		case 0:
			minX = controls[changed].x;
			minY = controls[changed].y;
			break;
		case 1:
			maxX = controls[changed].x;
			minY = controls[changed].y;
			break;
		case 2:
			maxX = controls[changed].x;
			maxY = controls[changed].y;
			break;
		case 3:
			minX = controls[changed].x;
			maxY = controls[changed].y;
			break;
		}
		updateControls();
	}
	
	public void updateControls() {
		controls[0].x = minX;
		controls[0].y = minY;
		
		controls[1].x = maxX;
		controls[1].y = minY;
				
		controls[2].x = maxX;
		controls[2].y = maxY;
				
		controls[3].x = minX;
		controls[3].y = maxY;
	}
	public int getSizeX() {
		return maxX - minX;
	}
	
	public int getSizeY() {
		return maxY - minY;
	}
	public int getMinX() {
		return minX;
	}

	public void setMinX(int minX) {
		this.minX = minX;
	}

	public int getMinY() {
		return minY;
	}

	public void setMinY(int minY) {
		this.minY = minY;
	}

	public int getMaxX() {
		return maxX;
	}

	public void setMaxX(int maxX) {
		this.maxX = maxX;
	}

	public int getMaxY() {
		return maxY;
	}

	public void setMaxY(int maxY) {
		this.maxY = maxY;
	}
}
