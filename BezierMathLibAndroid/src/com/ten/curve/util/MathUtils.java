package com.ten.curve.util;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.math.Vector2;
import com.ten.curve.CubicCurve;

public class MathUtils {

	public static boolean intersects(List<CubicCurve> curves, Vector2 start, Vector2 stop, float precision) {
		for (CubicCurve cc : curves)
			if (intersects(cc, start, stop, precision))
				return true;
		return false;
	}
	
	public static boolean intersects(CubicCurve cc, Vector2 start, Vector2 stop, float precision) {
		return intersects(cc.createPointList(precision, 0f), start, stop);
	}
	
	public static boolean intersects(List<Vector2> points, Vector2 start, Vector2 stop) {
		for (int i = 0; i < points.size() - 1; i++)
			if (intersects(points.get(i), points.get(i + 1), start, stop))
				return true;
		return false;
	}
	
	public static boolean intersects(Vector2 p1, Vector2 q1, Vector2 p2, Vector2 q2)
	{
		if (p1.equals(q1) || p2.equals(q2))
			return false;
		
	    // Find the four orientations needed for general and
	    // special cases
	    int o1 = orientation(p1, q1, p2);
	    int o2 = orientation(p1, q1, q2);
	    int o3 = orientation(p2, q2, p1);
	    int o4 = orientation(p2, q2, q1);
	 
	    // General case
	    if (o1 != o2 && o3 != o4)
	        return true;
	 
	    // Special Cases
	    // p1, q1 and p2 are colinear and p2 lies on segment p1q1
	    if (o1 == 0 && onSegment(p1, p2, q1)) return true;
	 
	    // p1, q1 and p2 are colinear and q2 lies on segment p1q1
	    if (o2 == 0 && onSegment(p1, q2, q1)) return true;
	 
	    // p2, q2 and p1 are colinear and p1 lies on segment p2q2
	    if (o3 == 0 && onSegment(p2, p1, q2)) return true;
	 
	     // p2, q2 and q1 are colinear and q1 lies on segment p2q2
	    if (o4 == 0 && onSegment(p2, q1, q2)) return true;
	 
	    return false; // Doesn't fall in any of the above cases
	}
	
	public static boolean onSegment(Vector2 l0, Vector2 l1, Vector2 p) {
		if (p.x <= Math.max(l0.x, l1.x) && p.x >= Math.min(l0.x, l1.x) && 
			p.y <= Math.max(l0.y, l1.y) && p.y >= Math.min(l0.y, l1.y))
			return true;
		else
			return false;
	}
	
	public static int orientation(Vector2 p, Vector2 q, Vector2 r) {
		float val = (q.y - p.y) * (r.x - q.x) -
	                (q.x - p.x) * (r.y - q.y);
	    if (val == 0.0f) 
	    	return 0;  // colinear
	    else
	    	return (val > 0) ? 1 : 2; // clock or counterclock wise
	}
	
	public static float fixAngle(float angle) {
		return angle % (float) (Math.PI * 2.0f);
	}
	
	public static float randNum(float range) {
		return ((float) Math.random() * range * 2.0f) - range;
	}
	
	public static Vector2 getValue(Vector2[] points, float t) {
		if (points.length == 1)
			return points[0];
		Vector2[] temp = new Vector2[points.length - 1];
		for (int i = 0; i < points.length - 1; i++)
			temp[i] = new Vector2(points[i])
					.add(new Vector2(points[i + 1])
					.sub(points[i]).mul(t));
		return getValue(temp, t);
	}
	
	public static Vector2 getTangent(Vector2[] points, float t) {
		if (points.length == 2)
			return new Vector2 (points[1]).sub(points[0]).nor();
		Vector2[] temp = new Vector2[points.length - 1];
		for (int i = 0; i < points.length - 1; i++)
			temp[i] = new Vector2(points[i])
					.add(new Vector2(points[i + 1])
					.sub(points[i]).mul(t));
		return getValue(temp, t);
	}
}
