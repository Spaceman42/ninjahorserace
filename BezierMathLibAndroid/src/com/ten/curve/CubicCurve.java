package com.ten.curve;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.math.Vector2;

public class CubicCurve {

	public Vector2 p0;
	public Vector2 p1;
	public Vector2 p2;
	public Vector2 p3;
	
	public CubicCurve() {
		p0 = new Vector2();
		p1 = new Vector2();
		p2 = new Vector2();
		p3 = new Vector2();
	}
	
	public CubicCurve(Vector2 p0, Vector2 p1, Vector2 p2, Vector2 p3) {
		this.p0 = p0;
		this.p1 = p1;
		this.p2 = p2;
		this.p3 = p3;
	}
	
	public Vector2 getValue(float t) {
		float a = (float) Math.pow(1 - t, 3.0);
		float b = 3 * t * (float) Math.pow(1 - t, 2.0);
		float c = 3 * (float) Math.pow(t, 2.0) * (1 - t);
		float d = (float) Math.pow(t, 3.0);
		return new Vector2(p0.x * a + p1.x * b + p2.x * c + p3.x * d,
				p0.y * a + p1.y * b + p2.y * c + p3.y * d);
	}
	
	public Vector2 getValue(float t, float offset) {
		return getValue(t).add(getNormal(t).mul(offset));
	}
	
	public Vector2 getTangent(float t) {
		float a = -3f * (float) Math.pow(1 - t, 2.0);
		float b = 3f * (3 * (float) Math.pow(t, 2) - 4 * t + 1);
		float c = 3f * (2 * t - 3 * (float) Math.pow(t, 2.0));
		float d = 3 * (float) Math.pow(t, 2.0);
		return new Vector2(p0.x * a + p1.x * b + p2.x * c + p3.x * d,
				p0.y * a + p1.y * b + p2.y * c + p3.y * d).nor();
	}

	public Vector2 getNormal(float t) {
		Vector2 tangent = getTangent(t);
		float temp = -tangent.x;
		tangent.x = tangent.y;
		tangent.y = temp;
		return tangent;
	}

	public List<Vector2> createPointList(float error, float offset) {
		List<Vector2> points = new ArrayList<Vector2>();
		for (float t = 0; t <= 1; t += error / calculateLength(error, 0, offset)) {
			points.add(getValue(t).add(getNormal(t).mul(offset)));
		}
		return points;
	}
	
	public void setPoint(int i, Vector2 point) {
		if (i == 0)
			p0 = point;
		else if (i == 1)
			p1 = point;
		else if (i == 2)
			p2 = point;
		else if (i == 3)
			p3 = point;
	}
	
	public float calculateParameter(float error, float distance) {
		return calculateParameter(error, distance, 0);
	}
	
	public float calculateParameter(float error, float distance, float start) {
		return calculateParameter(error, distance, start, 0);
	}
	
	public float calculateParameter(float error, float distance, float start, float offset) {
		float length = calculateLength(error, start, offset);
		float sum = 0.0f;
		int i = 0;
		for (float t = start; t <= 1 - (error / length); t += error / length) {
			sum += getRoughLength(t, t + error / length, offset);
			if (sum >= distance) {
				//System.out.println(i);
				return t + error / length;
			}
			i++;
		}
		return 1f;
	}
	
	public float calculateLength(float error) {
		return calculateLength(error, 0);
	}
	
	public float calculateLength(float error, float start) {
		return calculateLength(error, start, 1);
	}
	
	public float calculateLength(float error, float start, float offset) {
		return calculateLength(error, getRoughLength(start, 1, offset), start, 1, offset);
	}
	
	private float calculateLength(float error, float guess, float start, float stop, float offset) {
		float m = (stop + start) / 2.0f;
		float v1 = getRoughLength(start, m, offset);
		float v2 = getRoughLength(m, stop, offset);
		if (Math.abs(guess - (v1 + v2)) < error)
			return v1 + v2;
		else
			return calculateLength(error, v1, start, m, offset) +
					calculateLength(error, v2, m, stop, offset);
	}
	
	private float getRoughLength(float start, float stop, float offset) {
		return getValue(start, offset).sub(getValue(stop, offset)).len();
	}
}
