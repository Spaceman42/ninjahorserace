package com.ten;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

import org.andengine.util.debug.Debug;

import android.content.res.AssetManager;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Transform;

public class Utils {
	private Utils() {};
	
	private static Random rand = new Random();
	
	
	public static int randomInt(int lower, int upper) {
		int range = upper - lower;
		
		return rand.nextInt(range) + lower;
	}
	
	public static float randomFloat(float lower, float upper) {
		float range = upper = lower;
		
		return (rand.nextFloat()*range) + lower;
	}
	
	public static float[] toFloatArray(Vector2[] data) {
		float[] output = new float[data.length * 2];
		
		for (int i = 0; i<output.length; i+=2) {
			Vector2 vec = data[i/2];
			output[i] = vec.x;
			output[i+1] = vec.y;
		}
		return output;
		
	}
	
	public static String readFile(String filePath) throws IOException {
		AssetManager mngr = ResourceManager.getInstance().activity.getAssets();
			BufferedReader br = new BufferedReader(new InputStreamReader(mngr.open(filePath)));
			try {
				StringBuilder result = new StringBuilder();
				
				String line;
				while ((line = br.readLine()) != null) {
					result.append(line);
				}
				
				return result.toString();
			} finally {
				br.close();
			}
	}
	
	public static float getRotationFromTransform(Transform t) {
		return (float) Math.atan2(t.vals[t.COL1_Y], t.vals[t.COL2_Y]);
	}
	
	private static int ID = 0;
	
	public static synchronized int getNewID() {
		return ID++;
	}
}
