package com.ten;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public abstract class TiltListener implements SensorEventListener{

	public TiltListener() {
		GameActivity activity = ResourceManager.getInstance().activity;

		SensorManager sensorManager = (SensorManager) activity
				.getSystemService(Context.SENSOR_SERVICE);
		
		sensorManager.registerListener(this,
				sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
				SensorManager.SENSOR_DELAY_GAME);

	}
	@Override
	public void onSensorChanged(SensorEvent event) {
		if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
			float speedX = event.values[1];
			float speedY = event.values[0];
			onTiltUpdate(speedX, speedY);
		}
	}
	
	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
		
	}

	public abstract void onTiltUpdate(float tiltSpeedX, float tiltSpeedY);
}
