package com.ten;

import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.util.debug.Debug;

import android.os.AsyncTask;

import com.ten.scene.AbstractScene;
import com.ten.scene.LoadingScene;
import com.ten.scene.MainMenuScene;
import com.ten.scene.SplashScene;

public class SceneManager {

	private static final SceneManager INSTANCE = new SceneManager();

	private ResourceManager res = ResourceManager.getInstance();

	private AbstractScene currentScene;

	private LoadingScene loadingScene;

	private SceneManager() {
	}

	public static SceneManager getInstance() {
		return INSTANCE;
	}

	public void showSplash() {
		final SplashScene splash = new SplashScene();
		splash.loadResources();
		splash.create();
		setCurrentScene(splash);

		new AsyncTask<Void, Void, Void>() {

			@Override
			protected Void doInBackground(Void... params) {
				long timestamp = System.currentTimeMillis();
				//Load the default font
				//ResourceManager.getInstance().loadDefaultFont();
				// load menu scene for future use
				MainMenuScene menuScene = new MainMenuScene();
				menuScene.loadResources();
				menuScene.create();
				// load loading scene for future use
				loadingScene = new LoadingScene();
				loadingScene.loadResources();
				loadingScene.create();

				long timeElapsed = System.currentTimeMillis() - timestamp;
				if (timeElapsed < GameActivity.SPLASH_TIME_MS) {
					try {
						Thread.sleep(GameActivity.SPLASH_TIME_MS - timeElapsed);
					} catch (InterruptedException e) {
					}
				}
				setCurrentScene(menuScene);
				return null;
			}

		}.execute();
	}

	public void switchScene(final Class<? extends AbstractScene> sceneClass) {
		final AbstractScene oldScene = getCurrentScene();

		setCurrentScene(loadingScene);
		try {
			final AbstractScene scene = sceneClass.newInstance();

			new AsyncTask<Void, Void, Void>() {

				@Override
				protected Void doInBackground(Void... params) {
					if (oldScene != null) {
						oldScene.destroy();
						oldScene.unloadResources();
					}
					scene.loadResources();
					scene.create();
					
					setCurrentScene(scene);
					
					return null;
				}

			}.execute();
			res.camera.setHUD(null);
		    res.camera.setCenter(GameActivity.CAMX/2, GameActivity.CAMY/2);
		    res.camera.setRotation(0f);
		} catch (Exception e) {
			Debug.e("Scene instantiation failed", e);
			System.exit(-1);
		}
	}

	private void setCurrentScene(AbstractScene scene) {
		this.currentScene = scene;
		res.engine.setScene(currentScene);
	}

	public AbstractScene getCurrentScene() {
		return currentScene;
	}
}
