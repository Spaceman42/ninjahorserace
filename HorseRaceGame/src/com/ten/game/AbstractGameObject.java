package com.ten.game;

import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

public abstract class AbstractGameObject extends AnimatedSprite {

	protected Body body;

	public AbstractGameObject(float x, float y, float density, float elasticity, float friction,
			ITiledTextureRegion pTiledTextureRegion,
			VertexBufferObjectManager vbom, PhysicsWorld physicsWorld, String id) {
		super(x, y, pTiledTextureRegion, vbom);
		
		setUserData(id);
		createPhysics(physicsWorld, id, density, elasticity, friction);
	}

	private void createPhysics(PhysicsWorld physicsWorld, String id, float density, float elasticity, float friction) {
		body = PhysicsFactory.createBoxBody(physicsWorld, this,
				BodyType.DynamicBody, PhysicsFactory.createFixtureDef(density, elasticity, friction));

		body.setUserData(id);
		
		
		physicsWorld.registerPhysicsConnector(new PhysicsConnector(this, body, true, true)
	    {
	        @Override
	        public void onUpdate(float pSecondsElapsed)
	        {
	        	super.onUpdate(pSecondsElapsed);
	            onPhysicsWorldUpdate(pSecondsElapsed, body);
	        }
	    });
	}

	protected abstract void onPhysicsWorldUpdate(float secondsElapsed, Body body);
	
}
