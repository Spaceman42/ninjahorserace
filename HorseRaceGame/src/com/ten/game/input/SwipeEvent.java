package com.ten.game.input;

public class SwipeEvent {
	private final float originX;
	private final float originY;
	private final float finalX;
	private final float finalY;
	private final float deltaX;
	private final float deltaY;
	private final float direction;
	
	
	public SwipeEvent(float originX, float originY, float finalX, float finalY) {
		this.originX = originX;
		this.originY = originY;
		this.finalX = finalX;
		this.finalY = finalY;
		
		deltaX = finalX-originX;
		deltaY = finalY-originY;
		direction = (float)Math.atan2(deltaY, deltaX);
	}
	
	public float getLength() {
		return (float) Math.sqrt(deltaX*deltaX + deltaY*deltaY);
	}

	public float getOriginX() {
		return originX;
	}


	public float getOriginY() {
		return originY;
	}


	public float getFinalX() {
		return finalX;
	}


	public float getFinalY() {
		return finalY;
	}


	public float getDeltaX() {
		return deltaX;
	}


	public float getDeltaY() {
		return deltaY;
	}


	public float getDirection() {
		return direction;
	}
}
