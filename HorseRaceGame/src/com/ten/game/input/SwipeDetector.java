package com.ten.game.input;

import java.util.ArrayList;
import java.util.List;

import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.input.touch.TouchEvent;

public class SwipeDetector implements IOnSceneTouchListener{
	private float originX;
	private float originY;
	
	
	private List<SwipeListener> listeners;
	
	public SwipeDetector() {
		listeners = new ArrayList<SwipeListener>();
	}
	
	@Override
	public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {
		if (pSceneTouchEvent.isActionDown()) {
			originX = pSceneTouchEvent.getX();
			originY = pSceneTouchEvent.getY();
			
		} else if (pSceneTouchEvent.isActionMove()) {
			float finalX = pSceneTouchEvent.getX();
			float finalY = pSceneTouchEvent.getY();
			
			SwipeEvent e = new SwipeEvent(finalY, finalY, finalX, finalY);
			for (SwipeListener l : listeners) {
				l.onSwipe(e);
			}
		}
		return false;
	}
	
	public void registerListener(SwipeListener l) {
		listeners.add(l);
	}
	
	public boolean unregisterListener(SwipeListener l) {
		if (listeners.contains(l)) {
			listeners.remove(l);
			return true;
		}
		return false;
	}

}
