package com.ten.game.input;

public abstract class SwipeListener {
	public abstract void onSwipe(SwipeEvent e);
}
