package com.ten.game;

import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.debug.Debug;

import android.hardware.Sensor;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Transform;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.ten.ResourceManager;
import com.ten.TiltListener;
import com.ten.Utils;

import com.ten.game.input.SwipeEvent;
import com.ten.game.input.SwipeListener;
import com.ten.texture.AnimatedTexManager;
import com.ten.track.Track;
import com.ten.track.TrackFollower2;

public class Player extends AbstractHorse {
	
	private float tiltSpeedSide;
	private float tiltSpeedUp;
	
	public Player(float x, float y, AnimatedTexManager texture, Track track,
			float mapWidth, float mapHeight, final AbstractGameScene scene, VertexBufferObjectManager vbom,
			PhysicsWorld physicsWorld, String id) {
		super(x, y, texture, track, mapWidth, mapHeight, scene, vbom, physicsWorld, id);
		
		
		new TiltListener() {
			
			@Override
			public void onTiltUpdate(float tiltSpeedX, float tiltSpeedY) {
				tiltSpeedSide = tiltSpeedY;
				tiltSpeedUp = tiltSpeedX;
			}
		};
		
		scene.registerSwipeListener(new SwipeListener() {
			
			@Override
			public void onSwipe(SwipeEvent e) {
				
				
			}
		});
		
		ResourceManager.getInstance().camera.setChaseEntity(this);
		
		setSpeed(1.0f);
	}
	
	@Override
	public void onTick(float secondsElapsed) {
		ResourceManager.getInstance().camera.onUpdate(secondsElapsed);
		ResourceManager.getInstance().camera.setRotation(getRotation());
		
		changeOffset(tiltSpeedSide);
		
	}

	
}
