package com.ten.game;

import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Transform;
import com.ten.GameActivity;
import com.ten.TiltListener;
import com.ten.Utils;
import com.ten.texture.AnimatedTexManager;
import com.ten.track.Track;
import com.ten.track.TrackFollower2;

public abstract class AbstractHorse extends AbstractGameObject{
	
	private TrackFollower2 follower;
	
	private float offset;
	private final float maxOffset;
	
	
	private float speed;

	public AbstractHorse(float x, float y, AnimatedTexManager texture, Track track, float mapWidth, float mapHeight, AbstractGameScene scene,
			VertexBufferObjectManager vbom, PhysicsWorld physicsWorld, String id) {
		
		super(x, y, 1f, 0.5f, 0f, texture.getTextureRegion(), vbom, physicsWorld, id);
		
		this.follower = new TrackFollower2(track, 0);
		
		maxOffset = track.getOffset();
		offset = offset/2;
	
	}
	
	@Override
	protected void onPhysicsWorldUpdate(float secondsElapsed, Body body) {
		
		float ajustedSpeed = ajustValueForFPS(speed, secondsElapsed*1000);
		
		Transform trans = follower.nextTransform(ajustedSpeed, offset);
		
		body.setTransform(convertPixelsToMeters(trans.getPosition()), Utils.getRotationFromTransform(trans));
		
		onTick(secondsElapsed);
	}
	
	private float ajustValueForFPS(float v, float msElapsed) {
		return (v/GameActivity.TARGET_FRAME_TIME_MS)*msElapsed;
	}
	
	private Vector2 convertPixelsToMeters(Vector2 input) {
		float x = input.x/PhysicsConnector.PIXEL_TO_METER_RATIO_DEFAULT;
		float y = input.y/PhysicsConnector.PIXEL_TO_METER_RATIO_DEFAULT;
		return new Vector2(x, y);
	}
	
	public void setSpeed(float s) {
		this.speed = s;
	}
	
	public float getSpeed() {
		return speed;
	}
	
	public void setOffset(float o) {
		this.offset = o;
		if (offset > maxOffset) {
			offset = maxOffset;
		} else if (offset < 0) {
			offset = 0;
		}
	}
	
	public float getOffset() {
		return offset;
	}
	/**
	 * Identical to {@code setOffset(getOffset() + amount)}
	 * @param amount
	 */
	public void changeOffset(float amount) {
		setOffset(getOffset() + amount);
	}
	
	public abstract void onTick(float secondsElapsed);

}
