package com.ten.game;

import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.extension.tmx.TMXLayer;
import org.andengine.extension.tmx.TMXObject;
import org.andengine.extension.tmx.TMXObjectGroup;
import org.andengine.extension.tmx.TMXProperty;
import org.andengine.extension.tmx.TMXTiledMap;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.andengine.opengl.texture.atlas.buildable.builder.BlackPawnTextureAtlasBuilder;
import org.andengine.opengl.texture.atlas.buildable.builder.ITextureAtlasBuilder.TextureAtlasBuilderException;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.util.debug.Debug;

import com.ten.ResourceManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class GameMap {
	
	private static final String TRUE = "true";
	private static final int TYPE_PROPERTY_LOCATION = 0;
	private static final int IMAGE_PROPERTY_LOCATION = 1;
	private static final String TYPE_GATE = "gate";
	
	private TMXLayer[] layers;
	
	private BuildableBitmapTextureAtlas atlas;
	private List<MapObject> mapObjects;
	
	private float height;
	private float width;
	
	public GameMap(TMXTiledMap map, PhysicsWorld physicsWorld, String workingPath, int textureAtlasWidth, int textureAtlasHeight) {
		layers = new TMXLayer[map.getTMXLayers().size()];
		
		for (int i = 0; i<map.getTMXLayers().size(); i++) {
			TMXLayer layer = map.getTMXLayers().get(i);
			layer.detachSelf();
			layers[i] = layer;
		}
		
		setHeight(map.getHeight());
		setWidth(map.getWidth());
		
		mapObjects = new ArrayList<MapObject>();
		
		atlas = new BuildableBitmapTextureAtlas(
				ResourceManager.getInstance().activity.getTextureManager(),
				textureAtlasWidth, textureAtlasHeight);
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath(workingPath);
		for (TMXObjectGroup group : map.getTMXObjectGroups()) {
			if (group.getTMXObjectGroupProperties().containsTMXProperty(TYPE_GATE, TRUE)) {
				//TODO: load the gate here. It is special
			} else {
				TMXProperty typeProp = group.getTMXObjectGroupProperties().get(TYPE_PROPERTY_LOCATION);
				if (typeProp.getValue().equals(TRUE)) {
					String imageLoc = group.getTMXObjectGroupProperties().get(IMAGE_PROPERTY_LOCATION).getValue();
					
					ITextureRegion region = BitmapTextureAtlasTextureRegionFactory
							.createFromAsset(atlas, ResourceManager.getInstance().activity,
									imageLoc);
					
					for (TMXObject o : group.getTMXObjects()) {
						int x = o.getX();
						int y = (int) (map.getHeight() - o.getY());
						mapObjects.add(new MapObject(typeProp.getName(), region, physicsWorld, x, y));
					}
				}
			}
		}
		loadResources();
	}
	
	private void loadResources() {
		try {
			atlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(
					0, 1, 0));
		} catch (TextureAtlasBuilderException e) {
			Debug.e("Error building texture atlas for map!", e);
		}
		atlas.load();
	}
	
	public TMXLayer getLayer(int layer) {
		return layers[layer];
	}
	
	public void dispose() {
		atlas.unload();
		for (TMXLayer l : layers) {
			l.dispose();
		}
	}

	public List<MapObject> getMapObjects() {
		return mapObjects;
	}

	public float getHeight() {
		return height;
	}

	public void setHeight(float height) {
		this.height = height;
	}

	public float getWidth() {
		return width;
	}

	public void setWidth(float width) {
		this.width = width;
	}

}
