package com.ten.game;

import org.andengine.entity.sprite.Sprite;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.opengl.texture.region.ITextureRegion;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.ten.ResourceManager;

public class MapObject extends Sprite{
	public enum MapObjectType {
		SPEED_BOOST("speedboost");
		
		private String stringID;
		
		MapObjectType(String id) {
			stringID = id;
		}
		
		public String getStringID() {
			return stringID;
		}
		
		public static MapObjectType getFromString(String id) {
			for (MapObjectType t : values()) {
				if (t.getStringID().equals(id)) {
					return t;
				}
			}
			return null;
		}
	}
	
	private Body body;
	private final MapObjectType type;
	
	public MapObject(String name, ITextureRegion region, PhysicsWorld physicsWorld, int x, int y) {
		super(x, y, region, ResourceManager.getInstance().vbom);
		
		type = MapObjectType.getFromString(name);
		
		createPhysics(physicsWorld, name);
	}
	
	private void createPhysics(PhysicsWorld physicsWorld, String id) {
		body = PhysicsFactory.createBoxBody(physicsWorld, this,
				BodyType.StaticBody, PhysicsFactory.createFixtureDef(0, 0, 0));

		body.setUserData(id);
		
		for (Fixture f : body.getFixtureList()) {
			f.setSensor(true);
		}
		
		physicsWorld.registerPhysicsConnector(new PhysicsConnector(this, body, true, false)
	    {
	        @Override
	        public void onUpdate(float pSecondsElapsed)
	        {
	            onPhysicsWorldUpadtae(pSecondsElapsed, body);
	        }
	    });
	}
	
	private void onPhysicsWorldUpadtae(float pSecondsElapsed, Body body) {
		
	}
	
	public MapObjectType getType() {
		return type;
	}

}
