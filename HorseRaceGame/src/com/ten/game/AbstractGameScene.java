package com.ten.game;

import org.andengine.engine.camera.hud.HUD;
import org.andengine.input.touch.TouchEvent;

import com.ten.SceneManager;
import com.ten.game.input.SwipeDetector;
import com.ten.game.input.SwipeListener;
import com.ten.scene.AbstractScene;
import com.ten.scene.MainMenuScene;

public abstract class AbstractGameScene extends AbstractScene{
	protected GameMap map;
	protected HUD hud;
	
	private SwipeDetector detector;
	
	@Override
	public void create() {
		detector = new SwipeDetector();
		onMapCreate();
		
	}
	
	@Override
	public void unloadResources() {
		map.dispose();
		onUnloadResources();
	}


	@Override
	public void onBackKeyPressed() {
		SceneManager.getInstance().switchScene(MainMenuScene.class);
	}
	
	protected abstract GameMap onMapCreate();
	
	protected abstract void onUnloadResources();
	
	@Override
	public boolean onSceneTouchEvent(TouchEvent touchEvent) {
		boolean norm = super.onSceneTouchEvent(touchEvent);
		detector.onSceneTouchEvent(this, touchEvent);
		return norm;
	}
	
	public void registerSwipeListener(SwipeListener l) {
		detector.registerListener(l);
	}
	
	public boolean unregisterSwipeListener(SwipeListener l) {
		return detector.unregisterListener(l);
	}
}
