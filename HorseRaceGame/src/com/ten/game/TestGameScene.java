package com.ten.game;

import java.io.IOException;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.ten.GameActivity;
import com.ten.SceneManager;
import com.ten.Utils;

import org.andengine.engine.camera.hud.HUD;
import org.andengine.entity.primitive.Line;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.scene.background.SpriteBackground;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.extension.debugdraw.DebugRenderer;
import org.andengine.extension.physics.box2d.FixedStepPhysicsWorld;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.extension.tmx.TMXLayer;
import org.andengine.extension.tmx.TMXLoader;
import org.andengine.extension.tmx.TMXTiledMap;
import org.andengine.extension.tmx.util.exception.TMXLoadException;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.util.adt.color.Color;
import org.andengine.util.debug.Debug;
import org.xml.sax.SAXException;

import com.ten.SceneManager;
import com.ten.scene.AbstractScene;
import com.ten.scene.MainMenuScene;
import com.ten.texture.TextureAtlasManager;
import com.ten.track.TestTrack;
import com.ten.track.Track;
import com.ten.track.TrackDrawer;

public class TestGameScene extends AbstractGameScene {

	private TextureAtlasManager objectsAtlas;
	
	private PhysicsWorld physicsWorld;
	
	private Player player;
	private Track track;
	private Text hudText;
	
	private float mapHeight;
	private float mapWidth;
	
	private long startTime;
	private long endTime;
	private long frameTimes;
	private int frames;
	
	private float lastFrame;
	
	@Override
	public GameMap onMapCreate() {
		
		createBackground();
		createPhysics();
		createHUD();
		createTrack();
		/*
		attachChild(new Sprite(GameActivity.CAMX / 2,
				GameActivity.CAMY / 2, objectsAtlas.getTextureRegion("dirt"),
				vbom));
		*/
		hudText.setText(String.valueOf(1));
		hudText.setColor(Color.BLACK);
		
		TMXLoader tmxLoader = new TMXLoader(activity.getAssets(), activity.getTextureManager(), vbom);
		TMXTiledMap tmxMap = null;
		try {
			tmxMap = tmxLoader.loadFromAsset("map.tmx");
		} catch (TMXLoadException e) {
			Debug.e("TMX map loading failed!", e);
			System.exit(-1);
		}
		
		map = new GameMap(tmxMap, physicsWorld, "", 1024, 1024);
		mapHeight = map.getHeight();
		mapWidth = map.getWidth();
		attachChild(map.getLayer(0));
		
		for (MapObject o : map.getMapObjects()) {
			attachChild(o);
		}
		
		player = new Player(1, 1, objectsAtlas.getTiledTextureRegion("player"), track, mapWidth, mapHeight, this, vbom, physicsWorld, "player");
		
		player.setRotation(40);
		attachChild(player);
		
		TrackDrawer drawer = new TrackDrawer(track, mapWidth, mapHeight, vbom);
		
		for (Line l : drawer.getLines()) {
			attachChild(l);
		}
		
		DebugRenderer debugRender = new DebugRenderer(physicsWorld, vbom);
		attachChild(debugRender);
		
		for (PhysicsConnector p : physicsWorld.getPhysicsConnectorManager()) {
			Debug.e("Found physics Connecter: " + p.getBody().getUserData());
		}
		
		return map;
	}
	

	@Override
	public void onManagedUpdate(float t) {
		hudText.setText(String.valueOf(1000f/(t*1000)));
		super.onManagedUpdate(t);
		//lastFrame = t*1000;
	}
	
	private void startFrameTimer() {
		startTime = System.currentTimeMillis();
	}
	
	private void endFrameTimerAndWrite() {
		endTime = System.currentTimeMillis();
		
		frameTimes = frameTimes + endTime - startTime;
		
		frames++;
		
		if (frameTimes >= 1000) {
			hudText.setText(String.valueOf(frames));
			frames = 0;
			frameTimes = 0;
		}
	}

	
	
	private void createTrack() {
		track = new TestTrack();
		try {
			track.importFromXml(Utils.readFile("track.xml"));
		} catch (Exception e) {
			Debug.e("Importing track failed!", e);
		}
	}
	private void createHUD() {
		HUD hud = new HUD();
		hudText = new Text(200, 300, res.getDefaultFont(), "Debug info:", vbom);
		hud.attachChild(hudText);
		camera.setHUD(hud);
	}
	
	private void createBackground() {
		setBackground(new Background(Color.WHITE));
	}
	
	private void createPhysics() {
		physicsWorld = new FixedStepPhysicsWorld(60, new Vector2(0,0), false);
		
		registerUpdateHandler(physicsWorld);
		
		physicsWorld.setContactListener(new ContactListener() {
			
			@Override
			public void preSolve(Contact contact, Manifold oldManifold) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void postSolve(Contact contact, ContactImpulse impulse) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void endContact(Contact contact) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void beginContact(Contact contact) {
				System.exit(-1);
				
			}
		});
	}

	@Override
	public void destroy() {
		res.camera.setChaseEntity(null);

	}
	@Override
	protected void onUnloadResources() {
		objectsAtlas.unload();
	}



	@Override
	public void loadResources() {
		try {
			objectsAtlas = TextureAtlasManager.loadFromTAD(
					"gfx/game/objects/GameObjectsAtlasData.tad",
					"gfx/game/objects/");
		} catch (IOException e) {
			Debug.e("resource loading failed!", e);
		}
		
	}
}
