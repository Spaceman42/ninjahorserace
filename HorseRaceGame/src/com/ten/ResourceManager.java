package com.ten;

import org.andengine.engine.Engine;
import org.andengine.engine.camera.Camera;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import android.graphics.Color;
import android.hardware.SensorManager;
import android.view.GestureDetector;

public class ResourceManager {
	
	private static final ResourceManager instance = new ResourceManager();
	
	public GameActivity activity;
	public Engine engine;
	public Camera camera;
	public VertexBufferObjectManager vbom;
	
	private Font defaultFont;
	
	
	private ResourceManager() {
	}
	
	public static void init(Engine engine, GameActivity activity,
			Camera camera, VertexBufferObjectManager vbom) {
		getInstance().activity = activity;
		getInstance().engine = engine;
		getInstance().camera = camera;
		getInstance().vbom = vbom;
		
	}

	public Font loadFont(String base, String path) {
		FontFactory.setAssetBasePath(base);
		ITexture fontTexture = new BitmapTextureAtlas(
				activity.getTextureManager(), 256, 256,
				TextureOptions.BILINEAR_PREMULTIPLYALPHA);
		Font f = FontFactory.createStrokeFromAsset(activity.getFontManager(),
				fontTexture, activity.getAssets(), path,
				50, true, Color.WHITE, 2, Color.WHITE);
		f.load();		
		return f;
	}

	public static ResourceManager getInstance() {
		return instance;
	}
	
	public Font getDefaultFont() {
		if (defaultFont == null) {
			defaultFont = loadFont("font/pixel_art_font/", "pixelart.ttf");
		}
		return defaultFont;
	}
}
