package com.ten.texture;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.andengine.opengl.texture.atlas.buildable.builder.BlackPawnTextureAtlasBuilder;
import org.andengine.opengl.texture.atlas.buildable.builder.ITextureAtlasBuilder.TextureAtlasBuilderException;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.util.debug.Debug;
import org.andengine.util.level.simple.SimpleLevelLoader;

import android.content.res.AssetManager;

import com.ten.GameActivity;
import com.ten.ResourceManager;

public class TextureAtlasManager {
	private BuildableBitmapTextureAtlas textureAtlas;
	private Map<String, ITextureRegion> textureRegions;
	private Map<String, AnimatedTexManager> tiledTextureRegions;

	private TextureAtlasManager(BuildableBitmapTextureAtlas textureAtlas,
			Map<String, ITextureRegion> textureRegions,
			Map<String, AnimatedTexManager> tiledTextureRegions) {
		this.textureAtlas = textureAtlas;
		this.textureRegions = textureRegions;
		this.tiledTextureRegions = tiledTextureRegions;
	}

	public void unload() {
		textureAtlas.unload();
	}
	
	public AnimatedTexManager getTiledTextureRegion(String id) {
		return tiledTextureRegions.get(id);
	}
	
	public ITextureRegion getTextureRegion(String id) {
		return textureRegions.get(id);
	}

	/**
	 * creates a texture atlas from data found in a .tad file.
	 * 
	 * @param TADlocation
	 * @return
	 * @throws IOException
	 */
	public static TextureAtlasManager loadFromTAD(String TADlocation,
			String workingPath) throws IOException {
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath(workingPath);
		
		AssetManager mngr = ResourceManager.getInstance().activity.getAssets();
		BufferedReader br = new BufferedReader(new InputStreamReader(mngr.open(TADlocation)));
		
		try {
			int AtlasSize = getWidthAndHeight(br);

			BuildableBitmapTextureAtlas atlas = new BuildableBitmapTextureAtlas(
					ResourceManager.getInstance().activity.getTextureManager(),
					AtlasSize, AtlasSize);

			Map<String, ITextureRegion> textureRegions = new HashMap<String, ITextureRegion>();
			Map<String, AnimatedTexManager> tiledTextureRegions = new HashMap<String, AnimatedTexManager>();

			String line;
			while ((line = br.readLine()) != null) {
				if (line.startsWith("as")) {
					parseAnimatedSprite(line.split(" "), tiledTextureRegions,
							atlas);
				} else if (line.startsWith("s")) {
					parseSprite(line.split(" "), textureRegions, atlas);
				}
			}
			try {
				atlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(
						0, 1, 0));
			} catch (TextureAtlasBuilderException e) {
				Debug.e("Texture atlas building failed!",e);
				System.exit(-1);
			}
			atlas.load();
			return new TextureAtlasManager(atlas, textureRegions, tiledTextureRegions);
		} finally {
			br.close();
		}
	}

	private static void parseSprite(String[] data,
			Map<String, ITextureRegion> map,
			BuildableBitmapTextureAtlas atlas) {
		//s nameID image.png
		
		String id = data[1];
		String location = data[2];

		ITextureRegion region = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(atlas, ResourceManager.getInstance().activity,
						location);
		map.put(id, region);
	}

	private static void parseAnimatedSprite(String[] data,
			Map<String, AnimatedTexManager> map,
			BuildableBitmapTextureAtlas atlas) {
		//as nameID image.png rows columns
		
		String id = data[1];
		String location = data[2];
		int rows = Integer.parseInt(data[3]);
		int columns = Integer.parseInt(data[4]);

		ITiledTextureRegion region = BitmapTextureAtlasTextureRegionFactory
				.createTiledFromAsset(atlas,
						ResourceManager.getInstance().activity, location,
						columns, rows);

		map.put(id, new AnimatedTexManager(region, rows, columns));
	}

	private static int getWidthAndHeight(BufferedReader br) throws IOException {
		String line;
		while ((line = br.readLine()) != null) {
			if (line.startsWith("size")) {
				return Integer.parseInt(line.split("=")[1]);
			}
		}
		Debug.i("TAD file does not contain size!");
		return GameActivity.DEFAULT_TEXTURE_ATLAS_SIZE;
	}

}
