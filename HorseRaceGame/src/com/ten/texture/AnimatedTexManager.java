package com.ten.texture;

import java.util.Arrays;

import org.andengine.opengl.texture.region.ITiledTextureRegion;

public class AnimatedTexManager {
	
	private int rows;
	private int columns;
	
	private ITiledTextureRegion textureRegion;
	
	public AnimatedTexManager(ITiledTextureRegion textureRegion, int rows, int columns) {
		this.textureRegion = textureRegion;
		
		this.rows = rows;
		this.columns = columns;
	}
	
	public int getRows() {
		return rows;
	}
	
	public int getColumns() {
		return columns;
	}
	
	public ITiledTextureRegion getTextureRegion() {
		return textureRegion;
	}
	
	public int getNumberOfFrames() {
		return rows*columns;
	}
	
	public int[] getAnimationArray(int frametime) {
		int[] values = new int[rows*columns];
		Arrays.fill(values, frametime);
		return values;
	}
}
