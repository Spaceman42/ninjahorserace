package com.ten.scene;

import org.andengine.entity.scene.menu.MenuScene;
import org.andengine.entity.scene.menu.MenuScene.IOnMenuItemClickListener;
import org.andengine.entity.scene.menu.item.IMenuItem;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.andengine.opengl.texture.atlas.buildable.builder.BlackPawnTextureAtlasBuilder;
import org.andengine.opengl.texture.atlas.buildable.builder.ITextureAtlasBuilder.TextureAtlasBuilderException;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.util.debug.Debug;

import com.ten.GameActivity;
import com.ten.SceneManager;
import com.ten.game.TestGameScene;
import com.ten.scene.menu.AnimatedSpriteMenuItem;

public class MainMenuScene extends AbstractScene implements
		IOnMenuItemClickListener {

	private BuildableBitmapTextureAtlas texAtlas;
	
	private TextureRegion menuBackgroundRegion;
	private TiledTextureRegion playRegion;
	private TiledTextureRegion optionsRegion;

	private MenuScene menuChildScene;

	private AnimatedSpriteMenuItem playButton;
	private AnimatedSpriteMenuItem optionsButton;
	
	@Override
	public void create() {
		attachChild(new Sprite(GameActivity.CAMX / 2, GameActivity.CAMY / 2,
				menuBackgroundRegion, vbom));
		
		menuChildScene = new MenuScene(camera);
		playButton = new AnimatedSpriteMenuItem(playRegion, vbom);
		optionsButton = new AnimatedSpriteMenuItem(optionsRegion, vbom);
		
		menuChildScene.addMenuItem(playButton);
		menuChildScene.addMenuItem(optionsButton);
		
		menuChildScene.buildAnimations();
		menuChildScene.setBackgroundEnabled(false);
		menuChildScene.setOnMenuItemClickListener(this);

		optionsButton.setY(optionsButton.getY() - 100);
		
		setChildScene(menuChildScene);
	}

	@Override
	public boolean onMenuItemClicked(MenuScene pMenuScene, IMenuItem pMenuItem,
			float pMenuItemLocalX, float pMenuItemLocalY) {
		if (pMenuItem.getID() == playButton.getID()) {
			SceneManager.getInstance().switchScene(TestGameScene.class);
			return true;
		} else if (pMenuItem.getID() == optionsButton.getID()) {
			SceneManager.getInstance().switchScene(OptionsScene.class);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void destroy() {

	}

	@Override
	public void loadResources() {
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/menu/");
	
		texAtlas = new BuildableBitmapTextureAtlas(
				activity.getTextureManager(), 2048, 2048, TextureOptions.BILINEAR);
		
		menuBackgroundRegion = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(texAtlas, activity, "MenuBackground.png");
		
		playRegion = BitmapTextureAtlasTextureRegionFactory
				.createTiledFromAsset(texAtlas, activity.getAssets(), "play_buttons.png", 1, 2);
		optionsRegion = BitmapTextureAtlasTextureRegionFactory
				.createTiledFromAsset(texAtlas, activity.getAssets(), "options_buttons.png", 1, 2);
		
		try {
			texAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, 
					BitmapTextureAtlas>(0, 1, 0));
			texAtlas.load();
		} catch (final TextureAtlasBuilderException e) {
			Debug.e(e);
		}
	}

	@Override
	public void unloadResources() {
		texAtlas.unload();
	}

	@Override
	public void onBackKeyPressed() {
		System.exit(0);
	}
}
