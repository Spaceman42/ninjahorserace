package com.ten.scene;

import org.andengine.engine.Engine;
import org.andengine.engine.camera.Camera;
import org.andengine.entity.scene.Scene;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.ten.GameActivity;
import com.ten.ResourceManager;

public abstract class AbstractScene extends Scene {
	
	protected Engine engine;
    protected GameActivity activity;
    protected ResourceManager res;
    protected VertexBufferObjectManager vbom;
    protected Camera camera;
    
    public AbstractScene() {
    	this.res = ResourceManager.getInstance();
        this.engine = res.engine;
        this.activity = res.activity;
        this.vbom = res.vbom;
        this.camera = res.camera;    
    }
    
    /**
     * Called when constructed after {@code loadResources}
     */
    public abstract void create();
    
    public abstract void destroy();
    
    /**
     * Called before {@code create()} when constructed
     */
    public abstract void loadResources();
    
    public abstract void unloadResources();
    
    public abstract void onBackKeyPressed();
}
