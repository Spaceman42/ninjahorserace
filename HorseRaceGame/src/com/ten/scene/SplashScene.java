package com.ten.scene;

import org.andengine.entity.scene.background.Background;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.util.adt.color.Color;
import org.andengine.util.debug.Debug;

import com.ten.GameActivity;

public class SplashScene extends AbstractScene{

	private BitmapTextureAtlas splashTextureAtlas;
	private TextureRegion splash_region;
	
	private Sprite logo;

	@Override
	public void create() {
		setBackground(new Background(Color.WHITE));
		logo = new Sprite(GameActivity.CAMX/2, GameActivity.CAMX/2, splash_region, vbom);
		attachChild(logo);
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void loadResources() {
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
		splashTextureAtlas = new BitmapTextureAtlas(
				activity.getTextureManager(), 800, 600, TextureOptions.BILINEAR);
		splash_region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
				splashTextureAtlas, activity, "Logo.png", 0, 0);
		splashTextureAtlas.load();
	}

	@Override
	public void unloadResources() {
		splashTextureAtlas.unload();
		splash_region = null;
	}

	@Override
	public void onBackKeyPressed() {
		// TODO Auto-generated method stub
		
	}

}
