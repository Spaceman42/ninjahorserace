package com.ten.scene;

import org.andengine.entity.scene.background.Background;
import org.andengine.entity.text.Text;
import org.andengine.util.adt.color.Color;

import com.ten.GameActivity;

public class LoadingScene extends AbstractScene {

	@Override
	public void create() {
		setBackground(new Background(Color.BLACK));

		attachChild(new Text(GameActivity.CAMX / 2, GameActivity.CAMY / 2,
				res.getDefaultFont(), "Loading...", vbom));

	}

	@Override
	public void destroy() {
	}

	@Override
	public void loadResources() {

	}

	@Override
	public void unloadResources() {

	}

	@Override
	public void onBackKeyPressed() {

	}
}
