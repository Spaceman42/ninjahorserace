package com.ten.scene.menu;

import org.andengine.entity.scene.menu.item.IMenuItem;
import org.andengine.entity.scene.menu.item.SpriteMenuItem;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import com.ten.Utils;

public class AnimatedSpriteMenuItem extends AnimatedSprite implements IMenuItem {

	private int id = Utils.getNewID();
	
	public AnimatedSpriteMenuItem(ITiledTextureRegion region, 
			VertexBufferObjectManager vbom) {
		super(0, 0, region, vbom);
	}

	@Override
	public void onSelected() {
		setCurrentTileIndex(1);
	}

	@Override
	public void onUnselected() {
		setCurrentTileIndex(0);
	}
	
	@Override
	public int getID() {
		return id;
	}
}
