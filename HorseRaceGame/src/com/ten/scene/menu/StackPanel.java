package com.ten.scene.menu;

import org.andengine.entity.Entity;
import org.andengine.entity.IEntity;
import org.andengine.entity.scene.menu.item.IMenuItem;
import org.andengine.input.touch.TouchEvent;
import org.andengine.util.adt.list.SmartList;
import org.andengine.util.debug.Debug;

import android.view.MotionEvent;

public class StackPanel extends Entity {

	private float margin;
	private SmartList<IMenuItem> selectedItems = new SmartList<IMenuItem>();
	
	public StackPanel() {
		mChildren = new SmartList<IEntity>();
	}
	
	public float getMargin() {
		return margin;
	}
	
	public void setMargin(float margin) {
		this.margin = margin;
	}
	
	public SmartList<IEntity> getChildren() {
		return mChildren;
	}
	
	@Override
	public void attachChild(IEntity e) {
		super.attachChild(e);
		positionChildren();	
	}
	
	@Override
	public boolean detachChild(IEntity e) {
		boolean b = super.detachChild(e);
		positionChildren();
		return b;
	}
	
	public void positionChildren() {
		float total = 0;
		for (IEntity e : getChildren()) {
			float thisHeight = e.getHeight() + margin * 2.0f;
			total += thisHeight;
			e.setY((getHeight() / 2.0f) - (thisHeight / 2.0f) - total);
		}
		setWidth(calculateWidth());
		setHeight(calculateHeight());
	}

	public float calculateWidth() {
		float max = 0;
		for (IEntity e : getChildren())
			if (e.getWidth() > max)
				max = e.getWidth();
		// TODO Fix
		return 400;
	}
	
	public float calculateHeight() {
		float total = 0;
		for (IEntity e : getChildren())
			total += e.getHeight() + margin * 2.0f;
		// TODO Fix
		return 500;
	}
	
	@Override
	public boolean onAreaTouched(TouchEvent te, float localX, float localY) {
		Debug.i(Float.toString(te.getX()));
		Debug.i(Float.toString(te.getY()));
		
		for (IEntity e : getChildren()) {
			//e.onAreaTouched(te, localX - e.getX(), localY - e.getY());			
		}
		
		if (te.getAction() == MotionEvent.ACTION_MOVE) {
			
		} else if (te.getAction() == MotionEvent.ACTION_UP) {
			
		} else if (te.getAction() == MotionEvent.ACTION_CANCEL) {
			
		}
		
		/*
		 * switch (pSceneTouchEvent.getAction()) {
			case MotionEvent.ACTION_MOVE:
				if ((this.mSelectedMenuItem != null) && (this.mSelectedMenuItem != menuItem)) {
					this.mSelectedMenuItem.onUnselected();
				}
				this.mSelectedMenuItem = menuItem;
				this.mSelectedMenuItem.onSelected();
				break;
			case MotionEvent.ACTION_UP:
				if (this.mOnMenuItemClickListener != null) {
					final boolean handled = this.mOnMenuItemClickListener.onMenuItemClicked(this, 
					menuItem, pTouchAreaLocalX, pTouchAreaLocalY);
					menuItem.onUnselected();
					this.mSelectedMenuItem = null;
					return handled;
				}
				break;
			case MotionEvent.ACTION_CANCEL:
				menuItem.onUnselected();
				this.mSelectedMenuItem = null;
				break;
		}
		 */
		
		return false;
	}
}
