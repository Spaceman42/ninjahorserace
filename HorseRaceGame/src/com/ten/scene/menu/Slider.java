package com.ten.scene.menu;

import org.andengine.entity.Entity;
import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.debug.Debug;

import com.ten.scene.AbstractScene;

public class Slider extends Entity {
	
	private Sprite slider, thumb;

	public Slider(float pX, float pY, ITextureRegion sliderRegion, ITextureRegion thumbRegion,
			VertexBufferObjectManager vbom, final AbstractScene scene) {
	
		slider = new Sprite(pX, pY, sliderRegion, vbom) {
			
			@Override
			public boolean onAreaTouched(TouchEvent te, float localX, float localY) {
				thumb.setPosition(slider.getX() + localX - slider.getWidth() / 2, slider.getY());
				onSliderEvent(getValue());
				return false;
			}
		};
		
		thumb = new Sprite(slider.getX(), slider.getY(), thumbRegion, vbom);
		
		attachChild(slider);
		attachChild(thumb);
		
		scene.registerTouchArea(slider);
	}
	
	public float getValue() {
		return (thumb.getX() - slider.getX()) / slider.getWidth() + .5f;
	}
	
	public void onSliderEvent(float value) {
		
	}
}