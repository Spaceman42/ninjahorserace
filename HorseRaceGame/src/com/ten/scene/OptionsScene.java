package com.ten.scene;

import org.andengine.entity.scene.menu.MenuScene;
import org.andengine.entity.scene.menu.MenuScene.IOnMenuItemClickListener;
import org.andengine.entity.scene.menu.item.IMenuItem;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.Texture;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.andengine.opengl.texture.atlas.buildable.builder.BlackPawnTextureAtlasBuilder;
import org.andengine.opengl.texture.atlas.buildable.builder.ITextureAtlasBuilder.TextureAtlasBuilderException;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.util.adt.align.HorizontalAlign;
import org.andengine.util.debug.Debug;

import android.graphics.Color;

import com.ten.GameActivity;
import com.ten.SceneManager;
import com.ten.scene.menu.AnimatedSpriteMenuItem;
import com.ten.scene.menu.Slider;

public class OptionsScene extends AbstractScene implements 
		IOnMenuItemClickListener {

	private BuildableBitmapTextureAtlas texAtlas;
	
	private TextureRegion sliderBarRegion;
	private TextureRegion sliderThumbRegion;
	private TiledTextureRegion saveButtonRegion;
	private Font pixelFont;
	
	private Text sensText;
	private Slider sensSlider;
	private Text volumeText;
	private Slider volumeSlider;
	private IMenuItem saveButton;
	
	private float accelSensitivity;
	private float volume;
	
	@Override
	public void create() {
		MenuScene scene = new MenuScene(camera);
		
		sensText = new Text(GameActivity.CAMX / 2, GameActivity.CAMY / 2 + 600, pixelFont, "Sensitivity", 
				new TextOptions(HorizontalAlign.CENTER), vbom);
		sensSlider = new Slider(GameActivity.CAMX / 2, GameActivity.CAMY / 2 + 450, 
				sliderBarRegion, sliderThumbRegion, vbom, this) {
			
			@Override
			public void onSliderEvent(float value) {
				System.out.println("Sensitivity: " + value);
			}
		};
		
		volumeText = new Text(GameActivity.CAMX / 2, GameActivity.CAMY / 2 + 250, pixelFont, "Volume", 
				new TextOptions(HorizontalAlign.CENTER), vbom);
		volumeSlider = new Slider(GameActivity.CAMX / 2, GameActivity.CAMY / 2 + 100, 
				sliderBarRegion, sliderThumbRegion, vbom, this) {

			@Override
			public void onSliderEvent(float value) {
				System.out.println("Volume: " + value);
			}
		};
		
		saveButton = new AnimatedSpriteMenuItem(saveButtonRegion, vbom);
		
		scene.attachChild(sensText);
		scene.attachChild(sensSlider);
		scene.attachChild(volumeText);
		scene.attachChild(volumeSlider);
		scene.addMenuItem(saveButton);
		scene.buildAnimations();
		
		saveButton.setY(saveButton.getY() - 100);
		
		setChildScene(scene);
	}

	@Override
	public void destroy() {
	}

	@Override
	public void loadResources() {
		BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/menu/");
		texAtlas = new BuildableBitmapTextureAtlas(
				activity.getTextureManager(), 2048, 2048, TextureOptions.BILINEAR);
		sliderBarRegion = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(texAtlas, activity, "slider_track.png");
		sliderThumbRegion = BitmapTextureAtlasTextureRegionFactory
				.createFromAsset(texAtlas, activity, "slider_thumb.png");
		saveButtonRegion = BitmapTextureAtlasTextureRegionFactory
				.createTiledFromAsset(texAtlas, activity.getAssets(), "play_buttons.png", 1, 2);
		
		Texture fontTexture = new BitmapTextureAtlas(activity.getTextureManager(), 256, 256, TextureOptions.BILINEAR);
		FontFactory.setAssetBasePath("font/pixel_art_font/");
		pixelFont = FontFactory.createFromAsset(activity.getFontManager(), fontTexture, 
				activity.getAssets(), "pixelart.ttf", 72, true, Color.WHITE);
		
		try {
			texAtlas.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, 
					BitmapTextureAtlas>(0, 1, 0));
			texAtlas.load();
			pixelFont.load();
		} catch (final TextureAtlasBuilderException e) {
			Debug.e(e);
		}
	}

	@Override
	public void unloadResources() {
		texAtlas.unload();
	}

	@Override
	public void onBackKeyPressed() {
		SceneManager.getInstance().switchScene(MainMenuScene.class);
	}

	@Override
	public boolean onMenuItemClicked(MenuScene pMenuScene, IMenuItem pMenuItem,
			float pMenuItemLocalX, float pMenuItemLocalY) {
		if (pMenuItem == saveButton) {
			return true;
		} else {
			return false;
		}
	}
}
