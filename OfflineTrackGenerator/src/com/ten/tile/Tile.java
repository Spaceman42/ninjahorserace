package com.ten.tile;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.badlogic.gdx.math.Vector2;
import com.ten.track.Track;

public class Tile {
	private GID gid;
	
	
	public Tile(Vector2 upperLeft, int tileNum, Track track, float trackWidth, int sizePX) {
		float coverage = sampleTrack(track, trackWidth, upperLeft, sizePX, 4);
		
		if (coverage >= .5f) {
			gid = GID.TRACK;
		} else {
			gid = GID.GRASS_;
		}
		
	}
	
	public Tile(GID gid) {
		this.gid = gid;
	}
	
	public GID getGID() {
		return gid;
	}
	
	private float sampleTrack(Track track, float trackWidth, Vector2 upperLeft, int sizePX, int numberOfSamples) {
		Vector2[] samples = new Vector2[numberOfSamples];
		samples[0] = upperLeft;
		//System.out.println(upperLeft);
		samples[1] = new Vector2(upperLeft).add(new Vector2(sizePX, 0));
		samples[2] = new Vector2(upperLeft).add(new Vector2(sizePX, sizePX));
		samples[3] = new Vector2(upperLeft).add(new Vector2(0, sizePX));
		
		float[] sampleResults = new float[numberOfSamples];
		for (int i = 0; i<numberOfSamples; i++) {
			Vector2 vec = samples[i];
			if (track.isInsideTrack(vec, 4f, trackWidth)) {
				sampleResults[i] = 1.0f;
			} else {
				sampleResults[i] = 0.0f;
			}
		}
		return getAverage(sampleResults);
	}
	
	private float getAverage(float[] data) {
		float result = 0.0f;
		for (float f : data) {
			result += f;
		}
		result /= data.length;
		return result;
	}
	
	
	public Element getXMLElement(Document doc) {
		Element element = doc.createElement("tile");
		element.setAttribute("gid", String.valueOf(gid.getGID()));
		return element;
	}

	public void setGID(GID gid) {
		this.gid = gid;
	}
}
