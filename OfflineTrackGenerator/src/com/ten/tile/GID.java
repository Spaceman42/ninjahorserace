package com.ten.tile;

public enum GID {
	
	TRACK(21), GRASS_(1), GRASS_NO_LEFT(27), GRASS_NO_TOP_(25), GRASS_NO_RIGHT(28), GRASS_NO_BOTTOM_(37), 
		GRASS_NO_BOTTOM_NO_LEFT(17), GRASS_NO_BOTTOM_NO_RIGHT(18), GRASS_NO_TOP_NO_LEFT(5), GRASS_NO_TOP_NO_RIGHT(6);
	
	private int gid;
	
	GID(int gid) {
		this.gid = gid;
	}
	
	public int getGID() {
		return gid;
	}
	
	public static GID fromData(String name) {
		for (GID gid : values()) {
			if (gid.toString().equals(name)) {
				System.out.println("name: " + name);
				System.out.println("returning: " + gid.toString());
				return gid;
			}
		}
		return GRASS_;
	}
}
