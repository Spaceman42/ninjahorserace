package com.ten.tile;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class TMXGridExporter {
	

	private int gridWidth;
	private int gridHeight;
	
	private TileGrid tileGrid;

	public TMXGridExporter(TileGrid tileGrid) {
		this.tileGrid = tileGrid;
		
		this.gridWidth = tileGrid.getGridWidth();
		this.gridHeight = tileGrid.getGridHeight();
	}
	
	
	
	public void writeToFile(String fileLocation){
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			
			Document doc = docBuilder.newDocument();
			
			Element root = doc.createElement("map");
			attachMapAttr(root);
			doc.appendChild(root);
			
			Element tilesetData = doc.createElement("tileset");
			attachTilesetAttr(tilesetData);
			root.appendChild(tilesetData);
			
			Element imageSource = doc.createElement("imagesource");
			tilesetData.appendChild(imageSource);
			
			Element layer = doc.createElement("layer");
			attachLayerAttr(layer);
			root.appendChild(layer);
			
			Element data = doc.createElement("data");
			layer.appendChild(data);
			
			for (Tile t: tileGrid.getTiles()) {
				data.appendChild(t.getXMLElement(doc));
			}
			
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(fileLocation));
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.transform(source, result);
			
			System.out.println("File exported!");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	private void attachTilesetAttr(Element tilesetData) {
		tilesetData.setAttribute("firstgid", "1");
		tilesetData.setAttribute("name", "XXXX");
		tilesetData.setAttribute("tilewidth", "XXXX");
		tilesetData.setAttribute("tileheight", "XXXX");

	}



	private void attachLayerAttr(Element layer) {
		layer.setAttribute("name", "Background");
		layer.setAttribute("width", String.valueOf(gridWidth));
		layer.setAttribute("height", String.valueOf(gridHeight));
	}
	private void attachMapAttr(Element map) {
		map.setAttribute("version", "1.0");
		map.setAttribute("orientation", "orthogonal");
		map.setAttribute("width", String.valueOf(gridWidth));
		map.setAttribute("height", String.valueOf(gridHeight));
		map.setAttribute("tilewidth", "XXXX");
		map.setAttribute("tileheight", "XXXX");
	}

}
