package com.ten.tile;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.math.Vector2;
import com.ten.curve.CubicCurve;
import com.ten.track.Track;

public class TileGrid {
	private int gridWidth;
	private int gridHeight;
	
	private Tile[] tiles;
	
	private TileGrid(int gridWidth, int gridHeight, Tile[] tiles) {
		
		this.gridWidth = gridWidth;
		this.gridHeight = gridHeight;
		
		this.tiles = tiles;
	}
	
	
	
	public static TileGrid convertTrack2(Track track, float width, int targetNumTiles) {
		List<Line> lines = new ArrayList<Line>();
		
		List<CubicCurve> curves = track.getCurves();
		lines.addAll(generateInnerLines(curves));
		
		List<Vector2> outer = track.createPointList(1f, width);
		lines.addAll(generateOuterLines(outer));
		
		int tileSize = 800 / targetNumTiles;
		
		Tile[] tiles = new Tile[targetNumTiles*targetNumTiles];
		
		for (Line l : lines) {
			for (Vector2i point : l.rasterize()) {
				//Track tiles
				Vector2i tile = convertToTileCoord(point, tileSize);
				int tileNum = gridToArray(targetNumTiles, tile.x, tile.y);
				tiles[tileNum] = new Tile(GID.TRACK);
			}
		}
		
		for (int i = 0; i<targetNumTiles*targetNumTiles; i++) {
			if (tiles[i] == null) {
				tiles[i] = new Tile(GID.GRASS_);
			}
		}
		
		//********* Second pass. Create Edges. ********************///
		for (int i = 0; i < targetNumTiles*targetNumTiles; i++) {
			if (tiles[i].getGID() != GID.TRACK) {
				int[] coord = arrayToGrid(targetNumTiles, i);
				int x = coord[0];
				int y = coord[1];

				boolean topM = true;
				boolean rightM = true;
				boolean bottomM = true;
				boolean leftM = true;

				if (y > 0) {
					Tile top = tiles[gridToArray(targetNumTiles, x, y - 1)];
					topM = top.getGID() != GID.TRACK;
				}
				if (y < (targetNumTiles - 1)) {
					Tile bottom = tiles[gridToArray(targetNumTiles, x, y + 1)];
					bottomM = bottom.getGID() != GID.TRACK;
				}
				if (x > 0) {
					Tile left = tiles[gridToArray(targetNumTiles, x-1, y)];
					leftM = left.getGID() != GID.TRACK;
				}
				if (x < (targetNumTiles - 1)) {
					Tile right = tiles[gridToArray(targetNumTiles, x+1, y)];
					rightM = right.getGID() != GID.TRACK;
				}
				StringBuilder name = new StringBuilder("GRASS_");
				if (!topM) {
					name.append("NO_TOP_");
				}
				if (!bottomM) {
					name.append("NO_BOTTOM_");
				}
				if (!rightM) {
					name.append("NO_RIGHT");
				}
				if (!leftM) {
					name.append("NO_LEFT");
				}
				tiles[i].setGID(GID.fromData(name.toString()));
			}
		}
		
		////Third pass fill in dirt//
		
		for (int x = 0; x < targetNumTiles; x++) {
			boolean fill = false;
			for (int y = 0; y < targetNumTiles; y++) {
				Tile t = tiles[gridToArray(targetNumTiles, x, y)];
				if (isEdge(t)) {
					if (!fill) {
						fill = true;
					} else {
						fill = false;
					}
				} else {
					if (fill) {
						t.setGID(GID.TRACK);
					}
				}
			}
		}
		
		
		return new TileGrid(targetNumTiles, targetNumTiles, tiles);
	}
	
	private static boolean isEdge(Tile t) {
		GID g = t.getGID();
		if (g != GID.TRACK && g != GID.GRASS_) {
			return true;
		}
		return false;
	}
	
	private static Vector2 convertToScreenCoord(Vector2i tileCoord, int tileSize) {
		int x = tileCoord.x*tileSize - 400;
		int y = tileCoord.y*tileSize - 400;
		return new Vector2(x, y);
	}
	
	private static Vector2i convertToTileCoord(Vector2i screenCoord, int tileSize) {
		int x = (screenCoord.x+400)/tileSize;
		int y = (screenCoord.y+400)/tileSize;
		return new Vector2i(x, y);
	}
	
	private static List<Line> generateInnerLines(List<CubicCurve> curves) {
		List<Line> results = new ArrayList<Line>();
		for (CubicCurve c : curves) {
			List<Vector2> points = c.createPointList(1f);
			for (int i = 0; i < points.size() - 1; i++) {
				Vector2i start = new Vector2i((int) points.get(i).x, (int) points.get(i).y);
				Vector2i end = new Vector2i((int) points.get(i + 1).x, (int) points.get(i + 1).y);
				results.add(new Line(start, end));
			}
		}
		return results;
	}
	
	private static List<Line> generateOuterLines(List<Vector2> points) {
		List<Line> results = new ArrayList<Line>();
		for (int i = 0; i < points.size(); i++) {
			if (i == points.size() - 1) {
				Vector2i start = new Vector2i((int) points.get(i).x, (int) points.get(i).y); 
				Vector2i end = new Vector2i((int) points.get(0).x, (int) points.get(0).y);
				results.add(new Line(start, end));
			} else {
				Vector2i start = new Vector2i((int) points.get(i).x, (int) points.get(i).y); 
				Vector2i end = new Vector2i((int) points.get(i + 1).x, (int) points.get(i + 1).y);
				results.add(new Line(start, end));
			}
		}
		return results;
	}

	/**
	 * 
	 * @param track
	 * @param width
	 * @param numTiles
	 *            - this will be the largest number of tiles in a side. If the
	 *            map is wider than it is high the TMX will be numTiles wide.
	 * @return
	 */
	public static TileGrid covertTrack(Track track, float width, int numTiles) {

		float dx = track.getMaxX() - track.getMinX();
		float dy = track.getMaxY() - track.getMinY();

		System.out.println("dx: " + (int) dx);
		int tileGridWidth = 0;
		int tileGridHeight = 0;
		if (dx > dy) {
			tileGridWidth = numTiles;
			float ratio = (float) dy / (float) dx;
			tileGridHeight = (int) Math.ceil(numTiles * ratio);
		} else {
			tileGridHeight = numTiles;
			float ratio = (float) dx / (float) dy;
			tileGridWidth = (int) Math.ceil(numTiles * ratio);
		}

		int totalTiles = tileGridHeight * tileGridWidth;

		Tile[] tiles = new Tile[totalTiles];

		int tileSizePX = (int) (dx / numTiles);
		System.out.println("tileSize in pixels: " + tileSizePX);

		for (int i = 0; i < totalTiles; i++) {
			int[] coord = arrayToGrid(tileGridWidth, i);

			Vector2 upperLeft = convertToPX(coord, tileSizePX,
					(int) track.getMinX(), (int) track.getMinY());
			// System.out.println("Grid Coord: (" + coord[0] + ", " + coord[1] +
			// ") ");
			// System.out.println(upperLeft);

			tiles[i] = new Tile(upperLeft, i, track, width, tileSizePX);
		}

		for (int i = 0; i < totalTiles; i++) {
			if (tiles[i].getGID() != GID.TRACK) {
				int[] coord = arrayToGrid(tileGridWidth, i);
				int x = coord[0];
				int y = coord[1];

				boolean topM = true;
				boolean rightM = true;
				boolean bottomM = true;
				boolean leftM = true;

				if (y > 0) {
					Tile top = tiles[gridToArray(tileGridWidth, x, y - 1)];
					topM = top.getGID() != GID.TRACK;
				}
				if (y < (tileGridHeight - 1)) {
					Tile bottom = tiles[gridToArray(tileGridWidth, x, y + 1)];
					bottomM = bottom.getGID() != GID.TRACK;
				}
				if (x > 0) {
					Tile left = tiles[gridToArray(tileGridWidth, x-1, y)];
					leftM = left.getGID() != GID.TRACK;
				}
				if (x < (tileGridWidth - 1)) {
					Tile right = tiles[gridToArray(tileGridWidth, x+1, y)];
					rightM = right.getGID() != GID.TRACK;
				}
				StringBuilder name = new StringBuilder("GRASS_");
				if (!topM) {
					name.append("NO_TOP_");
				}
				if (!bottomM) {
					name.append("NO_BOTTOM_");
				}
				if (!rightM) {
					name.append("NO_RIGHT");
				}
				if (!leftM) {
					name.append("NO_LEFT");
				}
				tiles[i].setGID(GID.fromData(name.toString()));
			}
		}

		return new TileGrid(tileGridWidth, tileGridHeight, tiles);
	}
	
	private static Vector2 convertToPX(int[] gridCoord, int tileSize, int minX, int minY) {
		 int x = (gridCoord[0] * tileSize) + minX;
		 int y = (gridCoord[1] * tileSize) + minY;
		 return new Vector2(x, y);
	}
	
	private static int gridToArray(int gridWidth, int x, int y) {
		return x+(y*gridWidth);
	}
	
	private static int[] arrayToGrid(int gridWidth, int i) {
		int[] result = new int[2];
		
		result[0] = i % gridWidth;
		result[1] = i/gridWidth;
		
		return result;
	}
	
	public int getGridWidth() {
		return gridWidth;
	}
	
	public int getGridHeight() {
		return gridHeight;
	}
	
	public Tile[] getTiles() {
		return tiles;
	}
	
}
