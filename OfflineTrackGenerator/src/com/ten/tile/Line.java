package com.ten.tile;

import java.util.ArrayList;
import java.util.List;

public class Line {
	private Vector2i start;
	private Vector2i end;
	
	public Line(Vector2i start, Vector2i end) {
		this.setStart(new Vector2i(start));
		this.setEnd(new Vector2i(end));
	}

	public Vector2i getStart() {
		return start;
	}

	public void setStart(Vector2i start) {
		this.start = start;
	}

	public Vector2i getEnd() {
		return end;
	}

	public void setEnd(Vector2i end) {
		this.end = end;
	}
	
	
	public List<Vector2i> rasterize() {
		List<Vector2i> points = new ArrayList<Vector2i>();
		
		float x0 = start.x;
	    float y0 = start.y;
	    float x1 = end.x;
	    float y1 = end.y;
	    
	    //define vector differences and other variables required for Bresenham's Algorithm
	    float dx = Math.abs(x1 - x0);
	    float dy = Math.abs(y1 - y0);
	    
	    float sx = (x0 < x1) ? 1 : -1; //step x
	    float sy = (y0 < y1) ? 1 : -1; //step y
	    
	    float err = dx - dy; //get the initial error value
	    
	    //set the first point in the array
	    points.add(new Vector2i(x0, y0));
	    
	    //Main processing loop
	    
	    while(Math.abs(x1 - x0) >= 1.0f || Math.abs(y1 - y0) >= 1.0f) {
	        float e2 = err * 2; //hold the error value
	        
	        //use the error value to determine if the point should be rounded up or down
	        if(e2 >= -dy) {
	            err -= dy;
	            x0 += sx;
	        }
	        if(e2 < dx) {
	            err += dx;
	            y0 += sy;
	        }
	        
		    points.add(new Vector2i(x0, y0)); 
	    }		
		return points;
	}
}
