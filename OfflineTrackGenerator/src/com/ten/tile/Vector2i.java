package com.ten.tile;

public class Vector2i {
	public int x;
	public int y;
	
	public Vector2i() {
		
	}
	
	public Vector2i(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public Vector2i(float x, float y) {
		this.x = (int) x;
		this.y = (int) y;
	}
	
	public Vector2i(Vector2i other) {
		this.x = other.x;
		this.y = other.y;
	}
	
	public Vector2i add(Vector2i v) {
		x += v.x;
		y += v.y;
		return this;
	}
	
	public Vector2i sub(Vector2i v) {
		x -= v.x;
		y -= v.y;
		return this;
	}
	
	public Vector2i mul(Vector2i v) {
		x *= v.x;
		y *= v.y;
		return this;
	}
	
	public Vector2i div(Vector2i v) {
		x /= v.x;
		y /= v.y;
		return this;
	}
	
	public String toString() {
		return "{ " + x + ", " + y + " }";
	}
}
