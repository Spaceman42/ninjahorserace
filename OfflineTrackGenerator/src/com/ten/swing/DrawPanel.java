package com.ten.swing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.Timer;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import com.badlogic.gdx.math.Vector2;
import com.ten.curve.CubicCurve;
import com.ten.curve.CurveControl;
import com.ten.curve.util.BoundingBox;
import com.ten.curve.util.BoundingBox.ControlHandle;
import com.ten.curve.util.Side;
import com.ten.tile.Tile;
import com.ten.tile.Vector2i;
import com.ten.track.Track;
import com.ten.track.TrackGenerator;

public class DrawPanel extends JPanel implements MouseListener, MouseMotionListener, KeyListener {

	private float t = 0f;
	private float tOuter = 0f; 
	//private float offset = 40f;
	
	private TrackGenerator trackGen;
	private BoundingBox boundingBox;
	
	private CurveControl selectedCurveControl;
	private CurveControl selectedCurveControlBack;
	private CurveControl selectedCurveControlFront;
	
	
	private int numTiles;
	private int tileSize;
	
	public DrawPanel() {
		setFocusable(true);
		requestFocusInWindow();
		addMouseListener(this);
		addMouseMotionListener(this);
		addKeyListener(this);

		trackGen = new TrackGenerator();
		regenerateTrack();
		boundingBox = new BoundingBox(trackGen.getTrack());
		trackGen.getTrack().setOffset(40);
		
	}
	
	public void setNumTiles(int i) {
		numTiles = i;
	}
	
	public void regenerateTrack() {
		trackGen.createInitialTrack(10, 200);
	}
	
	public Track getTrack() {
		return trackGen.getTrack();
	}
	
	public void setTrack(String xml) throws SAXException, IOException, ParserConfigurationException {
		trackGen.getTrack().importFromXml(xml);
	}
	
	protected void paintComponent(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		g2.clearRect(0, 0, getWidth(), getHeight());
		g2.translate(400, 400);
		g2.setColor(Color.BLUE);
		
		drawTileGrid(g2);
		
		trackGen.regenerateTrack();
		trackGen.getTrack().updateBound(boundingBox);

		List<CubicCurve> curves = getTrack().getCurves();
		drawCurves(g2, curves);
		
		List<Vector2> outer = getTrack().createPointList(1f, getTrack().getOffset());
		drawLoop(g2, outer);

		for (CurveControl cc : trackGen.getControls()) {
			drawPoint(g2, cc.controlPoint, 6);
			drawLine(g2, cc.getEndpointBack(), cc.controlPoint);
			drawPoint(g2, cc.getEndpointBack(), 6);
			drawLine(g2, cc.getEndpointFront(), cc.controlPoint);
			drawPoint(g2, cc.getEndpointFront(), 6);
		}
		
		t = getTrack().calculateParameter(2f, t, 0);
		tOuter = getTrack().calculateParameter(2f, tOuter, getTrack().getOffset());
		
		drawPoint(g2, getTrack().getValue(t), 6);
		drawPoint(g2, getTrack().getValue(tOuter, getTrack().getOffset()), 6);
		boundingBox.updateControls();
		drawBoundingBox(g2, boundingBox);
		
	}
	
	public float getOffset() {
		return getTrack().getOffset();
	}
	
	public int getNumTiles() {
		return numTiles;
	}
	
	private void drawTileGrid(Graphics2D g2) {
		if (numTiles > 0) {
			tileSize = 800 / numTiles;

			for (int x = 0; x < numTiles; x++) {
				for (int y = 0; y < numTiles; y++) {
					Vector2i loc = convertToScreenCoord(new Vector2i(x, y));
					loc = convertToTileCoord(loc);
					loc = convertToScreenCoord(loc);
					g2.drawRect(loc.x, loc.y,
							tileSize, tileSize);
				}
			}
		}

	}
	
	private Vector2i convertToScreenCoord(Vector2i tileCoord) {
		int x = tileCoord.x*tileSize - 400;
		int y = tileCoord.y*tileSize - 400;
		return new Vector2i(x, y);
	}
	
	private Vector2i convertToTileCoord(Vector2i screenCoord) {
		int x = (screenCoord.x+400)/tileSize;
		int y = (screenCoord.y+400)/tileSize;
		return new Vector2i(x, y);
	}
	
	public int getTileSize() {
		return tileSize;
	}

	public static void drawBoundingBox(Graphics2D g2, BoundingBox bb) {

		Color oldColor = g2.getColor();
		g2.setColor(Color.GREEN);

		g2.drawLine(bb.getMinX(), bb.getMinY(), bb.getMaxX(), bb.getMinY());
		g2.drawLine(bb.getMinX(), bb.getMinY(), bb.getMinX(), bb.getMaxY());
		g2.drawLine(bb.getMinX(), bb.getMaxY(), bb.getMaxX(), bb.getMaxY());
		g2.drawLine(bb.getMaxX(), bb.getMinY(), bb.getMaxX(), bb.getMaxY());
		drawBoundingBoxControlHandles(g2, bb);
		g2.setColor(oldColor);

	}

	public static void drawBoundingBoxControlHandles(Graphics2D g2, BoundingBox bb) {
		for (ControlHandle ch : bb.getControls()) {
			g2.fillOval(ch.x-ch.size/2, ch.y-ch.size/2, ch.size, ch.size);
		}
	}
	
	private static void drawCurves(Graphics2D g2, List<CubicCurve> curves) {
		for (CubicCurve c : curves) {
			List<Vector2> points = c.createPointList(1f);
			for (int i = 0; i < points.size() - 1; i++) {
				g2.drawLine((int) points.get(i).x, (int) points.get(i).y, 
						(int) points.get(i + 1).x, (int) points.get(i + 1).y);
			}
		}
	}
	
	private static void drawLoop(Graphics2D g2, List<Vector2> points) {
		for (int i = 0; i < points.size(); i++) {
			if (i == points.size() - 1)
				g2.drawLine((int) points.get(i).x, (int) points.get(i).y, 
						(int) points.get(0).x, (int) points.get(0).y);
			else
				g2.drawLine((int) points.get(i).x, (int) points.get(i).y, 
						(int) points.get(i + 1).x, (int) points.get(i + 1).y);
		}
	}
	
	private static void drawPoint(Graphics2D g2, Vector2 p, int radius) {
		g2.fillOval((int) p.x - radius, (int) p.y - radius, radius * 2, radius * 2);
	}
	
	private static void drawLine(Graphics2D g2, Vector2 p0, Vector2 p1) {
		g2.drawLine((int) p0.x, (int) p0.y, (int) p1.x, (int) p1.y);
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		selectedCurveControl = trackGen.getNearestCurveControl(
				new Vector2(arg0.getX() - 400, arg0.getY() - 400), 6f);
		selectedCurveControlBack = trackGen.getNearestCurveControlBack(
				new Vector2(arg0.getX() - 400, arg0.getY() - 400), 6f);
		selectedCurveControlFront = trackGen.getNearestCurveControlFront(
				new Vector2(arg0.getX() - 400, arg0.getY() - 400), 6f);
		
		boundingBox.setClickedControl(arg0.getX()-400, arg0.getY()-400);
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		selectedCurveControl = null;
	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		if (selectedCurveControlFront != null) { 
			Vector2 v = new Vector2(arg0.getX() - 400, arg0.getY() - 400)
					.sub(selectedCurveControlFront.controlPoint);
			Vector2 n = new Vector2(v).nor();
			selectedCurveControlFront.controlAngle = 
					(float) Math.atan2(n.y, n.x);
			selectedCurveControlFront.controlLength2 = v.len();
		} else if (selectedCurveControlBack != null) { 
			Vector2 v = new Vector2(arg0.getX() - 400, arg0.getY() - 400)
					.sub(selectedCurveControlBack.controlPoint);
			Vector2 n = new Vector2(v).nor().mul(-1f);
			selectedCurveControlBack.controlAngle = 
					(float) Math.atan2(n.y, n.x);
			selectedCurveControlBack.controlLength1 = v.len();
		} else if (selectedCurveControl != null) {
			selectedCurveControl.controlPoint = 
					new Vector2(arg0.getX() - 400, arg0.getY() - 400);
		}
		boundingBox.mouseDragged(arg0.getX()-400, arg0.getY()-400);
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		if (arg0.getKeyCode() == KeyEvent.VK_UP) {
			getTrack().setOffset(getTrack().getOffset() + 10);
		} else if (arg0.getKeyCode() == KeyEvent.VK_DOWN) {
			getTrack().setOffset(getTrack().getOffset() - 10);	
		}
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
	}
}
