package com.ten.swing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.sound.midi.Track;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import com.ten.tile.TMXGridExporter;
import com.ten.tile.TileGrid;

public class FileMenu extends JMenu{

	private JMenuItem exportButton;
	private JMenuItem saveButton;
	private JMenuItem loadButton;
	
	private DrawPanel drawPanel;

	public FileMenu(DrawPanel d) {
		super("File");
		createFileMenu();
		
		this.drawPanel = d;
	}

	private void createFileMenu() {
		
		(saveButton = new JMenuItem("Save")).addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				onSave();
			}
		});
		
		(loadButton = new JMenuItem("Load")).addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				onLoad();
			}
		});
		(exportButton = new JMenuItem("Export")).addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				onExport();
			}
		});
		
		add(saveButton);
		add(loadButton);
		add(exportButton);
	}
	
	private void onSave() {
		try {
			Utils.writeToFile("TMXMaps/track.xml", drawPanel.getTrack().exportToXml());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void onLoad() {
		try {
			String data = Utils.readFromFile("TMXMaps/track.xml");
			drawPanel.setTrack(data);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void onExport() {
		TileGrid grid = TileGrid.convertTrack2(drawPanel.getTrack(), drawPanel.getOffset(), drawPanel.getNumTiles());
		TMXGridExporter exporter = new TMXGridExporter(grid);
		exporter.writeToFile("TMXMaps/map.tmx");
	}

}
