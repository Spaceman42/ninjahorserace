package com.ten.swing;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class SidePanel extends JPanel{

	
	private JTextField numTiles;
	private JLabel tileSize;
	
	public SidePanel(final DrawPanel d) {
		super(new BorderLayout());
		JButton regenButton = new JButton("Regenerate Track");
		add(regenButton, BorderLayout.EAST);
		
		regenButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				d.regenerateTrack();
				
			}
		});
		
		numTiles = new JTextField(3);
		add(numTiles, BorderLayout.SOUTH);
		numTiles.setText("0");
		
		tileSize = new JLabel("TileSize = 0");
		add(tileSize, BorderLayout.NORTH);

	}
	
	public int getNumTiles() {
		return Integer.parseInt(numTiles.getText());
	}
	
	public void setTileSize(int newSize) {
		tileSize.setText("TileSize = " + newSize);
	}

}
