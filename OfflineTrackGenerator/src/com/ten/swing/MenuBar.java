package com.ten.swing;



import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class MenuBar extends JMenuBar {

	private JFileChooser fileChooser;
	
	private FileMenu fileMenu;
	
	
	
	public MenuBar(DrawPanel d) {
		fileMenu = new FileMenu(d);
		
		add(fileMenu);
	}
	
	public FileMenu getFileMenu() {
		return fileMenu;
	}
	
	

}
