package com.ten.swing;

import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Window extends JFrame{

	private JPanel panel;

	public Window() {
		setSize(1024, 768);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		panel = new JPanel(new BorderLayout());
		panel.setToolTipText("Main Panel");
		add(panel);

		
	}
	public void addToPanel(Component c, String location) {
		panel.add(c, location);
	}

}
