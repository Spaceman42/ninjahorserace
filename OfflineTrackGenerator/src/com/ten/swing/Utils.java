package com.ten.swing;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Utils {
	private Utils() {
	}

	public static void writeToFile(String filePath, String data)
			throws IOException {
		
			BufferedWriter wr = new BufferedWriter(new FileWriter(new File(
					filePath)));
		try {
			wr.write(data);
		} finally {
			wr.close();
		}
	}
	
	public static String readFromFile(String filePath) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(new File(filePath)));
		
		try {
			StringBuilder data = new StringBuilder();
			
			String line;
			while((line = br.readLine()) != null) {
				data.append(line);
			}
			
			return data.toString();
		} finally {
			br.close();
		}
	}
}
