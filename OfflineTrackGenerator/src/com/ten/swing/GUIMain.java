package com.ten.swing;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.Timer;

public class GUIMain {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		GUIMain gui = new GUIMain();
	}
	
	
	public GUIMain() {
		Window w = new Window();
		
		final DrawPanel d= new DrawPanel();
		w.addToPanel(d, BorderLayout.CENTER);
		d.setPreferredSize(new Dimension(800,800));
		w.addToPanel(new MenuBar(d), BorderLayout.NORTH);
		
		final SidePanel p = new SidePanel(d);
		
		w.addToPanel(p, BorderLayout.EAST);
		
		Timer updateTimer = new Timer(16, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				d.setNumTiles(p.getNumTiles());
				d.repaint();
				p.setTileSize(d.getTileSize());
			}
		});
		updateTimer.start();
		w.pack();
		//w.setResizable(false);
		w.setVisible(true);
	}

}
